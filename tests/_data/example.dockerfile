ARG something

# some really important commands
RUN ls -la

# actually important
CMD composer install && \
    start.sh
