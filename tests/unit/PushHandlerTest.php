<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Model\EventModel;
use App\Model\HookModel;
use App\Model\PushEventCollectionModel;
use App\Model\PushEventModel;
use App\Model\ReferenceStateModel;
use App\Model\Repository;
use App\Service\ProjectManager;
use App\Service\PushHandler;
use Codeception\Test\Unit;

class PushHandlerTest extends Unit
{
    private $allowedBranches = [];

    public function testHandlesEventModelCorrectly()
    {
        $handler = $this->makeService();

        $model = new EventModel();
        $model->repository = new Repository();
        $model->repository->name = 'repository';
        $model->actor = 'stub';
        $model->push = new PushEventCollectionModel();
        $model->push->changes = [
            $this->makePushEventModel('branch1', ReferenceStateModel::TYPE_BRANCH),
            $this->makePushEventModel('branch2', ReferenceStateModel::TYPE_BRANCH),
            $this->makePushEventModel('branch3', ReferenceStateModel::TYPE_TAG),
            $this->makePushEventModel('branch1', ReferenceStateModel::TYPE_BRANCH),
        ];

        $this->allowedBranches = array_flip([
            'branch1',
            'branch2'
        ]);
        $handler->handle($model);
    }

    private function makeService(): PushHandler
    {
        $projectManager = $this->makeEmpty(ProjectManager::class, [
            'update' => function (HookModel $model) {
                self::assertEquals('repository', $model->getProject());
                self::assertTrue(isset($this->allowedBranches[$model->getBranch()]));
                unset($this->allowedBranches[$model->getBranch()]);
            }
        ]);

        return new PushHandler('foo', $projectManager);
    }

    private function makePushEventModel(string $branch, string $type): PushEventModel
    {
        $event = new PushEventModel;
        $event->new = new ReferenceStateModel;
        $event->new->name = $branch;
        $event->new->type = $type;

        return $event;
    }
}
