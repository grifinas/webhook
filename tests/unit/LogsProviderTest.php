<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Model\HookLogRequestModel;
use App\Model\HookModel;
use App\Service\FileSystem;
use App\Service\LogsProvider;
use App\Strategy\Log\LogsStrategyInterface;
use App\Strategy\LogsStrategyProvider;
use Codeception\Test\Unit;

class LogsProviderTest extends Unit
{
    private const LINE_NUM_OUTPUT = 1000;

    public function testGetHookLogs()
    {
        $model = new HookLogRequestModel();
        $model->branch = 'branch';
        $model->project = 'project';
        $model->repository = 'repository';

        $logsStrategyProvider = $this->createMock(LogsStrategyProvider::class);
        $logsStrategyProvider
            ->expects(self::once())
            ->method('get')
            ->willReturnCallback(function (HookModel $hook) use ($model) {
                $this->assertEquals($model->project, $hook->getName());
                $this->assertEquals($model->repository, $hook->getProject());
                $this->assertEquals($model->branch, $hook->getBranch());
                $this->assertEquals('foo', $hook->getRoot());
            })
            ->willReturn($this->createMock(LogsStrategyInterface::class))
        ;

        $provider = new LogsProvider(
            'foo',
            'bar',
            $this->createMock(FileSystem::class),
            $logsStrategyProvider,
        );

        $provider->getHookLogs($model);
    }

    public function testGetWebhookLog()
    {
        $branchRoot = 'root';
        $logPath = '/log/path';
        $logs = "log";

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::once())
            ->method('tail')
            ->with($logPath, self::LINE_NUM_OUTPUT)
            ->willReturn($logs)
        ;

        $provider = new LogsProvider(
            $branchRoot,
            $logPath,
            $fs,
            $this->createMock(LogsStrategyProvider::class),
        );

        $providerLogs = $provider->getWebhookLog();
        $this->assertEquals('[log]', $providerLogs);
    }
}
