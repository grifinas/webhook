<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Exception\StrategyNotFoundException;
use App\Model\HookModel;
use App\Service\FileSystem;
use App\Strategy\Log\K8SLogsStrategy;
use App\Strategy\LogsStrategyProvider;
use Codeception\Test\Unit;

class LogsStrategyProviderTest extends Unit
{
    public function testGetSuccess()
    {
        $hook = (new HookModel('project', 'branch'))->setRoot('foo');

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::once())
            ->method('isDir')
            ->with("{$hook->getRoot()}/k8s")
            ->willReturn(true)
        ;

        $provider = new LogsStrategyProvider(
            $fs,
            $this->createMock(K8SLogsStrategy::class),
        );

        $provider->get($hook);
    }

    public function testGetFail()
    {
        $hook = (new HookModel('project', 'branch'))->setRoot('foo');

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::once())
            ->method('isDir')
            ->with("{$hook->getRoot()}/k8s")
            ->willReturn(false)
        ;

        $this->expectException(StrategyNotFoundException::class);

        $provider = new LogsStrategyProvider(
            $fs,
            $this->createMock(K8SLogsStrategy::class),
        );

        $provider->get($hook);
    }
}
