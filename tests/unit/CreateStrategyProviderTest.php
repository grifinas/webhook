<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Exception\StrategyNotFoundException;
use App\Model\HookModel;
use App\Service\FileSystem;
use App\Strategy\Create\CreateStrategyInterface;
use App\Strategy\Create\K8SCreateStrategy;
use App\Strategy\Create\SS2ApiCreateStrategy;
use App\Strategy\Create\SS2SpaCreateStrategy;
use App\Strategy\Create\UpsAdapterCreateStrategy;
use App\Strategy\CreateStrategyProvider;
use App\Strategy\StrategySelector;
use Codeception\Test\Unit;

class CreateStrategyProviderTest extends Unit
{
    private const REPOSITORY_SS2_API = 'shipment-system-api';
    private const REPOSITORY_SS2_SPA = 'shipment-system-spa';
    private const REPOSITORY_UPS_ADAPTER = 'ups-adapter';

    public function testGetStrategySuccess()
    {
        $hook = new HookModel('project', 'branch');
        $hook->setRoot('foo');

        $ss2Api = $this->createMock(SS2ApiCreateStrategy::class);
        $ss2Spa = $this->createMock(SS2SpaCreateStrategy::class);
        $upsDeAdapter = $this->createMock(UpsAdapterCreateStrategy::class);

        $selector = $this->createMock(StrategySelector::class);
        $selector
            ->expects(self::once())
            ->method('select')
            ->willReturnCallback(function (HookModel $model, $strategies) use ($hook, $ss2Api, $ss2Spa, $upsDeAdapter) {
                $this->assertEquals($hook, $model);
                $this->assertEquals([
                    self::REPOSITORY_SS2_API => $ss2Api,
                    self::REPOSITORY_SS2_SPA => $ss2Spa,
                    self::REPOSITORY_UPS_ADAPTER => $upsDeAdapter,
                ], $strategies);

                return $this->createMock(CreateStrategyInterface::class);
            })
        ;

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::never())
            ->method('isDir')
        ;

        $provider = new CreateStrategyProvider(
            $selector,
            $fs,
            $this->createMock(K8SCreateStrategy::class),
            $ss2Api,
            $ss2Spa,
            $upsDeAdapter,
        );

        $provider->get($hook);
    }

    public function testGetSuccessWhenStrategyNotFound()
    {
        $hook = new HookModel('project', 'branch');
        $hook->setRoot('foo');

        $ss2Api = $this->createMock(SS2ApiCreateStrategy::class);
        $ss2Spa = $this->createMock(SS2SpaCreateStrategy::class);
        $upsDeAdapter = $this->createMock(UpsAdapterCreateStrategy::class);

        $selector = $this->createMock(StrategySelector::class);
        $selector
            ->expects(self::once())
            ->method('select')
            ->willReturnCallback(function (HookModel $model, $strategies) use ($hook, $ss2Api, $ss2Spa, $upsDeAdapter) {
                $this->assertEquals($hook, $model);
                $this->assertEquals([
                    self::REPOSITORY_SS2_API => $ss2Api,
                    self::REPOSITORY_SS2_SPA => $ss2Spa,
                    self::REPOSITORY_UPS_ADAPTER => $upsDeAdapter,
                ], $strategies);
                throw new StrategyNotFoundException;
            })
        ;

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::once())
            ->method('isDir')
            ->with("{$hook->getRoot()}/k8s")
            ->willReturn(true)
        ;

        $provider = new CreateStrategyProvider(
            $selector,
            $fs,
            $this->createMock(K8SCreateStrategy::class),
            $ss2Api,
            $ss2Spa,
            $upsDeAdapter,
        );

        $provider->get($hook);
    }

    public function testGetStrategyFail()
    {
        $hook = new HookModel('project', 'branch');
        $hook->setRoot('foo');

        $ss2Api = $this->createMock(SS2ApiCreateStrategy::class);
        $ss2Spa = $this->createMock(SS2SpaCreateStrategy::class);
        $upsDeAdapter = $this->createMock(UpsAdapterCreateStrategy::class);

        $selector = $this->createMock(StrategySelector::class);
        $selector
            ->expects(self::once())
            ->method('select')
            ->willReturnCallback(function (HookModel $model, $strategies) use ($hook, $ss2Api, $ss2Spa, $upsDeAdapter) {
                $this->assertEquals($hook, $model);
                $this->assertEquals([
                    self::REPOSITORY_SS2_API => $ss2Api,
                    self::REPOSITORY_SS2_SPA => $ss2Spa,
                    self::REPOSITORY_UPS_ADAPTER => $upsDeAdapter,
                ], $strategies);
                throw new StrategyNotFoundException;
            })
        ;

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::once())
            ->method('isDir')
            ->with("{$hook->getRoot()}/k8s")
            ->willReturn(false)
        ;

        $this->expectException(StrategyNotFoundException::class);

        $provider = new CreateStrategyProvider(
            $selector,
            $fs,
            $this->createMock(K8SCreateStrategy::class),
            $ss2Api,
            $ss2Spa,
            $upsDeAdapter,
        );

        $provider->get($hook);
    }
}
