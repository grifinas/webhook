<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Model\HookModel;
use App\Model\KubernetesPodModel;
use App\Service\FileSystem;
use App\Service\Kubernetes;
use App\Service\KubernetesFileSystem;
use App\Strategy\Delete\K8SDeleteStrategy;
use Codeception\Test\Unit;

class K8SDeleteStrategyTest extends Unit
{
    private const CONFIG_DIRS = [
        'k8s/dev',
    ];
    private const YAML_CONFIG_ORDER = [
        'secrets',
        'deployment',
        'services',
        'ingress',
    ];
    private const DEV_NAMESPACE = 'namespace: venipak';

    public function testDeleteConfiguration()
    {
        $name = 'name';
        $configs = ['conf_00', 'conf_01', 'conf_02'];

        $hook = (new HookModel('project', 'branch'))
            ->setRoot('foo')
            ->setName($name)
        ;

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::once())
            ->method('isDir')
            ->with($hook->getRoot())
            ->willReturn(true)
        ;

        $pod = new KubernetesPodModel('name', 'Running', 'namespace');

        $kube = $this->createMock(Kubernetes::class);
        $kube
            ->expects(self::at(0))
            ->method('findPod')
            ->with($hook->getName(), $hook->getBranch())
            ->willReturn($pod)
        ;
        $kube
            ->expects(self::at(1))
            ->method('findPod')
            ->with($hook->getName(), $hook->getBranch())
            ->willReturn($pod)
        ;
        $kube
            ->expects(self::once())
            ->method('getNamespacePods')
            ->with($pod->namespace)
            ->willReturn(['pod-00', 'pod-01', 'pod-01'])
        ;
        $kube
            ->expects(self::atLeastOnce())
            ->method('delete')
            ->willReturnCallback(function (string $config) use ($configs) {
                $this->assertTrue(in_array($config, $configs));
            })
        ;

        $kubeFs = $this->createMock(KubernetesFileSystem::class);
        $kubeFs
            ->expects(self::once())
            ->method('getConfigs')
            ->with($hook->getRoot(), self::YAML_CONFIG_ORDER, self::CONFIG_DIRS)
            ->willReturn($configs)
        ;
        $kubeFs
            ->expects(self::atLeastOnce())
            ->method('makeYaml')
            ->willReturnCallback(function (HookModel $model, string $config) use ($hook, $configs) {
                $this->assertTrue(in_array($config, $configs));
            })
        ;

        $strategy = new K8SDeleteStrategy(
            $fs,
            $kube,
            $kubeFs,
        );

        $strategy->delete($hook);
    }

    public function testDeleteFullNamespace()
    {
        $name = 'name';

        $hook = (new HookModel('project', 'branch'))
            ->setRoot('foo')
            ->setName($name)
        ;

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::once())
            ->method('isDir')
            ->with($hook->getRoot())
            ->willReturn(true)
        ;

        $pod = new KubernetesPodModel('name', 'Running', 'namespace');

        $kube = $this->createMock(Kubernetes::class);
        $kube
            ->expects(self::at(0))
            ->method('findPod')
            ->with($hook->getName(), $hook->getBranch())
            ->willReturn($pod)
        ;
        $kube
            ->expects(self::at(1))
            ->method('findPod')
            ->with($hook->getName(), $hook->getBranch())
            ->willReturn($pod)
        ;
        $kube
            ->expects(self::once())
            ->method('getNamespacePods')
            ->with($pod->namespace)
            ->willReturn(['pod-00'])
        ;
        $kube
            ->expects(self::once())
            ->method('deleteNamespace')
            ->with($pod->namespace)
        ;

        $strategy = new K8SDeleteStrategy(
            $fs,
            $kube,
            $this->createMock(KubernetesFileSystem::class),
        );

        $strategy->delete($hook);
    }
}
