<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Model\ExecResponseModel;
use App\Service\Docker;
use App\Service\ShellExecutor;
use Codeception\Test\Unit;

class DockerTest extends Unit
{
    public function testBuild(): void
    {
        $docker = $this->makeService(static function ($command, $arguments) {
            self::assertEquals('docker build -t $target -f $file $path --build-arg $bk_this=$bv_this', $command);
            self::assertEquals([
                'target' => 'localhost:32000/foo',
                'file' => 'bar',
                'path' => 'baz',
                'bk_this' => 'this',
                'bv_this' => 'test'
            ], $arguments);

            return new ExecResponseModel(0, []);
        });

        $docker->build('foo', 'bar', 'baz', [
            'this' => 'test'
        ]);
    }

    public function testPush(): void
    {
        $docker = $this->makeService(static function ($command, $arguments) {
            self::assertEquals('docker push $target', $command);
            self::assertEquals([
                'target' => 'localhost:32000/foo',
            ], $arguments);

            return new ExecResponseModel(0, []);
        });

        $docker->push('foo');
    }

    private function makeService(callable $exec): Docker
    {
        $executor = $this->makeEmpty(ShellExecutor::class, [
            'exec' => $exec
        ]);

        return new Docker($executor);
    }
}
