<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Exception\StrategyNotFoundException;
use App\Model\HookModel;
use App\Strategy\StrategySelector;
use Codeception\Test\Unit;

class StrategySelectorTest extends Unit
{
    public function testSelectSuccess()
    {
        $hook = new HookModel('project', 'branch');

        $strategies = ['project' => 'foo'];

        $selector = new StrategySelector;

        $strategy = $selector->select($hook, $strategies);
        $this->assertEquals('foo', $strategy);
    }

    public function testSelectFail()
    {
        $hook = new HookModel('project', 'branch');

        $strategies = ['foo' => 'bar'];

        $this->expectException(StrategyNotFoundException::class);

        $selector = new StrategySelector;

        $selector->select($hook, $strategies);
    }
}
