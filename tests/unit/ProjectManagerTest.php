<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Model\HookModel;
use App\Service\FileSystem;
use App\Service\Git;
use App\Service\ProjectManager;
use App\Service\WebhookCache;
use App\Strategy\CreateStrategyProvider;
use App\Strategy\Delete\DeleteStrategyInterface;
use App\Strategy\Create\CreateStrategyInterface;
use App\Strategy\DeleteStrategyProvider;
use App\Strategy\Update\UpdateStrategyInterface;
use App\Strategy\UpdateStrategyProvider;
use Codeception\Test\Unit;

class ProjectManagerTest extends Unit
{
    public function testCreate()
    {
        $project = 'project';
        $branch = 'branch';

        $hook = new HookModel($project, $branch);
        $hook->setRoot('foo');

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::once())
            ->method('mkdir')
            ->with(sprintf('%s/%s', $hook->getRoot(), $hook->getProject()))
        ;
        $fs
            ->expects(self::once())
            ->method('isDir')
            ->with($hook->getRoot())
            ->willReturn(false)
        ;

        $git = $this->createMock(Git::class);
        $git
            ->expects(self::once())
            ->method('clone')
            ->with($hook->getProject(), $hook->getBranch(), $hook->getRoot())
        ;

        $createInterface = $this->createMock(CreateStrategyInterface::class);
        $createInterface
            ->expects(self::once())
            ->method('create')
            ->with($hook)
            ->willReturn($hook)
        ;

        $createStrat = $this->createMock(CreateStrategyProvider::class);
        $createStrat
            ->expects(self::once())
            ->method('get')
            ->with($hook)
            ->willReturn($createInterface)
        ;

        $cache = $this->createMock(WebhookCache::class);
        $cache
            ->expects(self::once())
            ->method('put')
            ->with($hook)
        ;

        $manager = new ProjectManager(
            'foo',
            $git,
            $fs,
            $cache,
            $createStrat,
            $this->createMock(UpdateStrategyProvider::class),
            $this->createMock(DeleteStrategyProvider::class),
        );

        $manager->update($hook);
    }

    public function testUpdate()
    {
        $project = 'project';
        $branch = 'branch';

        $hook = new HookModel($project, $branch);
        $hook->setRoot('foo');

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::once())
            ->method('mkdir')
            ->with(sprintf('%s/%s', $hook->getRoot(), $hook->getProject()))
        ;
        $fs
            ->expects(self::once())
            ->method('isDir')
            ->with($hook->getRoot())
            ->willReturn(true)
        ;

        $git = $this->createMock(Git::class);
        $git
            ->expects(self::once())
            ->method('pull')
            ->with($hook->getBranch(), $hook->getRoot())
        ;

        $updateInterface = $this->createMock(UpdateStrategyInterface::class);
        $updateInterface
            ->expects(self::once())
            ->method('update')
            ->with($hook)
            ->willReturn($hook)
        ;

        $updateStrat = $this->createMock(UpdateStrategyProvider::class);
        $updateStrat
            ->expects(self::once())
            ->method('get')
            ->with($hook)
            ->willReturn($updateInterface)
        ;

        $cache = $this->createMock(WebhookCache::class);
        $cache
            ->expects(self::once())
            ->method('put')
            ->with($hook)
        ;

        $manager = new ProjectManager(
            'foo',
            $git,
            $fs,
            $cache,
            $this->createMock(CreateStrategyProvider::class),
            $updateStrat,
            $this->createMock(DeleteStrategyProvider::class),
        );

        $manager->update($hook);
    }

    public function testRemove()
    {
        $project = 'project';
        $branch = 'branch';

        $hook = new HookModel($project, $branch);
        $hook->setRoot('foo');

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::once())
            ->method('isDir')
            ->with($hook->getRoot())
            ->willReturn(true)
        ;
        $fs
            ->expects(self::once())
            ->method('rename')
        ;

        $cache = $this->createMock(WebhookCache::class);
        $cache
            ->expects(self::once())
            ->method('delete')
            ->with($hook)
        ;

        $deleteStrat = $this->createMock(DeleteStrategyProvider::class);
        $deleteStrat
            ->expects(self::once())
            ->method('get')
            ->willReturn($this->createMock(DeleteStrategyInterface::class))
        ;

        $manager = new ProjectManager(
            'foo',
            $this->createMock(Git::class),
            $fs,
            $cache,
            $this->createMock(CreateStrategyProvider::class),
            $this->createMock(UpdateStrategyProvider::class),
            $deleteStrat,
        );

        $manager->remove($hook);
    }
}
