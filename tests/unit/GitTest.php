<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Exception\FileNotFoundException;
use App\Model\ExecResponseModel;
use App\Service\Git;
use App\Service\ShellExecutor;
use Codeception\Test\Unit;

class GitTest extends Unit
{
    public function testClone(): void
    {
        $git = $this->makeService(static function ($command, $arguments) {
            self::assertEquals('git clone --single-branch --branch $branch $repository $dir', $command);
            self::assertEquals([
                'repository' => 'git@host:owner/foo.git',
                'branch' => 'bar',
                'dir' => 'baz',
            ], $arguments);

            return new ExecResponseModel(0, []);
        });

        $git->clone('foo', 'bar', 'baz');
    }

    public function testPull(): void
    {
        $git = $this->makeService(static function ($command, $arguments) {
            self::assertEquals('cd $dir && git fetch && git reset --hard $branch', $command);
            self::assertEquals([
                'branch' => 'origin/foo',
                'dir' => 'bar',
            ], $arguments);

            return new ExecResponseModel(0, []);
        });

        $git->pull('foo', 'bar');
    }

    public function testGetRepositoryBranchesSuccess()
    {
        $execRespModel = new ExecResponseModel(0, ['master']);

        $shellExecutor = $this->createMock(ShellExecutor::class);
        $shellExecutor
            ->expects(self::at(0))
            ->method('exec')
            ->willReturnCallback(function ($cmd, $args) use ($execRespModel) {
                $this->assertEquals('ls $dir', $cmd);
                $this->assertEquals(['dir' => 'dir/repo'], $args);
                return $execRespModel;
            })
        ;
        $shellExecutor
            ->expects(self::at(1))
            ->method('exec')
            ->willReturnCallback(function ($cmd, $args) {
                $this->assertEquals(['dir' => 'dir/repo/master'], $args);
                return new ExecResponseModel(0, ['foo']);
            })
        ;

        $git = new Git('host', 'owner', $shellExecutor);
        $git->getRepositoryBranches('repo', 'dir');
    }

    public function testGetRepositoryBranchesNoFileOrDir()
    {
        $execRespModel = new ExecResponseModel(0, ['No such file or directory']);

        $shellExecutor = $this->createMock(ShellExecutor::class);
        $shellExecutor
            ->expects(self::at(0))
            ->method('exec')
            ->willReturnCallback(function ($cmd, $args) use ($execRespModel) {
                $this->assertEquals('ls $dir', $cmd);
                $this->assertEquals(['dir' => 'dir/repo'], $args);
                return $execRespModel;
            })
        ;

        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage('No such file or directory');

        $git = new Git('host', 'owner', $shellExecutor);
        $git->getRepositoryBranches('repo', 'dir');
    }

    public function testGetRepositoryBranchesNoMaster()
    {
        $execRespModel = new ExecResponseModel(0, ['foo']);

        $shellExecutor = $this->createMock(ShellExecutor::class);
        $shellExecutor
            ->expects(self::at(0))
            ->method('exec')
            ->willReturnCallback(function ($cmd, $args) use ($execRespModel) {
                $this->assertEquals('ls $dir', $cmd);
                $this->assertEquals(['dir' => 'dir/repo'], $args);
                return $execRespModel;
            })
        ;

        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage('No master branch found');

        $git = new Git('host', 'owner', $shellExecutor);
        $git->getRepositoryBranches('repo', 'dir');
    }

    private function makeService(callable $exec): Git
    {
        $executor = $this->makeEmpty(ShellExecutor::class, [
            'exec' => $exec
        ]);

        return new Git('host', 'owner', $executor);
    }
}
