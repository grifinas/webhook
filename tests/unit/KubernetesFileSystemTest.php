<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Model\HookModel;
use App\Service\FileSystem;
use App\Service\KubernetesFileSystem;
use Codeception\Test\Unit;

class KubernetesFileSystemTest extends Unit
{
    public function testGetConfDirs()
    {
        $kfs = new KubernetesFileSystem($this->createMock(FileSystem::class));

        $dirs = $kfs->getConfigDirs('a', ['b', 'c']);

        $this->assertTrue(in_array('a/b', $dirs));
        $this->assertTrue(in_array('a/c', $dirs));
    }

    public function testGetLink()
    {
        $kfs = new KubernetesFileSystem($this->createMock(FileSystem::class));

        $hook = (new HookModel('project', 'branch'))->setName('name');

        $this->assertEquals('branch.name.venipak.megodata', $kfs->getLink($hook));
    }
}
