<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Service\ShellExecutor;
use Codeception\Test\Unit;
use Psr\Log\LoggerInterface;

class ShellExecutorTest extends Unit
{
    public function testExec(): void
    {
        $command = 'ls $dir';
        $args = [
            'dir' => '/var'
        ];

        $executor = $this->makeService(static function (string $message, array $data) use ($command, $args) {
            self::assertNotEmpty($data['output']);

            self::assertEquals([
                'command' => $command,
                'args' => $args,
                // This part is correct only for linux
                'sanitized' => "ls '/var' 2>&1",
                'response' => 0,
                'output' => $data['output'],
            ], $data);
        });

        // Actual exec runs in this test, be careful with commands sent
        $executor->exec($command, $args);
    }

    private function makeService(callable $asserts): ShellExecutor
    {
        $logger = $this->createMock(LoggerInterface::class);
        $logger
            ->expects(self::once())
            ->method('info')
            ->willReturnCallback($asserts)
        ;

        return new ShellExecutor($logger);
    }
}
