<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Model\HookModel;
use App\Model\KubernetesPodModel;
use App\Service\FileSystem;
use App\Service\Kubernetes;
use App\Strategy\Log\K8SLogsStrategy;
use Codeception\Test\Unit;

class K8SLogsStrategyTest extends Unit
{
    public function testGetLogs()
    {
        $hook = (new HookModel('project', 'branch'))
            ->setRoot('foo')
            ->setName('name')
        ;

        $pod = new KubernetesPodModel('name', 'Running', 'namespace');

        $kube = $this->createMock(Kubernetes::class);
        $kube
            ->expects(self::once())
            ->method('findPod')
            ->with($hook->getName(), $hook->getBranch())
            ->willReturn($pod)
        ;
        $kube
            ->expects(self::once())
            ->method('getContainerLog')
            ->with($pod, 'bar')
            ->willReturn(['name'])
        ;

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::once())
            ->method('get')
            ->with("{$hook->getRoot()}/k8s/dev/deployment.yaml")
            ->willReturn($this->mockYamlContent())
        ;

        $strategy = new K8SLogsStrategy(
            $kube,
            $fs,
        );

        $strategy->getLogs($hook);
    }

    private function mockYamlContent(): string
    {
        return "
spec: 
  template: 
    spec: 
      containers: 
        - name: bar
        ";
    }
}
