<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Model\HookModel;
use App\Service\Docker;
use App\Service\FileSystem;
use App\Service\Kubernetes;
use App\Service\KubernetesFileSystem;
use App\Strategy\Create\SS2SpaCreateStrategy;
use Codeception\Test\Unit;

class SS2SpaCreateStrategyTest extends Unit
{
    public function testSpaBuildImages()
    {
        $token = 'xxx-111-!!!';

        $hook = (new HookModel('project', 'branch'))
            ->setRoot('foo')
            ->setName('bar')
        ;

        $docker = $this->createMock(Docker::class);
        $docker
            ->expects(self::once())
            ->method('build')
            ->with("{$hook->getName()}:dev", "{$hook->getRoot()}/docker/Dockerfile.dev", $hook->getRoot(), [
                'FA_TOKEN' => $token
            ])
        ;
        $docker
            ->expects(self::once())
            ->method('push')
            ->with("{$hook->getName()}:dev")
        ;

        $kubeFs = $this->createMock(KubernetesFileSystem::class);
        $kubeFs
            ->expects(self::once())
            ->method('getProjectName')
            ->with($hook)
            ->willReturn($hook->getName())
        ;

        $strategy = new SS2SpaCreateStrategy(
            $token,
            $this->createMock(Kubernetes::class),
            $docker,
            $this->createMock(FileSystem::class),
            $kubeFs,
        );

        $strategy->create($hook);
    }
}
