<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Entity\Webhook;
use App\Exception\WebhookHandlerException;
use App\Model\HookModel;
use App\Repository\WebhookRepository;
use App\Service\FileSystem;
use App\Service\Git;
use App\Service\ProjectManager;
use App\Service\WebhookService;
use App\Transformer\WebhookTransformer;
use Codeception\Test\Unit;

class WebhookServiceTest extends Unit
{
    public function testGetBranches()
    {
        $repo = 'repo';
        $branchDir = 'branch';

        $output = ['br0', 'br1', 'br2'];

        $git = $this->createMock(Git::class);
        $git
            ->expects(self::once())
            ->method('getRepositoryBranches')
            ->with($repo, $branchDir)
            ->willReturn($output)
        ;

        $webhookService = new WebhookService(
            $branchDir,
            $this->createMock(FileSystem::class),
            $this->createMock(ProjectManager::class),
            $git,
            $this->createMock(WebhookRepository::class),
            $this->createMock(WebhookTransformer::class),
        );

        $result = $webhookService->getBranches($repo);
        $this->assertEquals($output, $result);
    }

    public function testGetProjects()
    {
        $branchDir = 'dir';
        $project = ['app1'];

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::once())
            ->method('getDirContents')
            ->with($branchDir)
            ->willReturnCallback(function () use ($project) {
                foreach ($project as $app) {
                    yield $app;
                }
            })
        ;

        $webhookService = new WebhookService(
            $branchDir,
            $fs,
            $this->createMock(ProjectManager::class),
            $this->createMock(Git::class),
            $this->createMock(WebhookRepository::class),
            $this->createMock(WebhookTransformer::class),
        );

        $result = $webhookService->getProjects();
        $this->assertEquals($project, $result);
    }

    public function testCreateSuccess()
    {
        $repo = 'repo';
        $branch = 'branch';
        $baseDir = 'foo';

        $repository = $this->createMock(WebhookRepository::class);
        $repository
            ->expects(self::once())
            ->method('findOneBy')
            ->with([
                'branch' => $branch,
                'project' => $repo,
            ])
            ->willReturn(null)
        ;

        $manager = $this->createMock(ProjectManager::class);
        $manager
            ->expects(self::once())
            ->method('update')
            ->willReturnCallback(function (HookModel $model) use ($repo, $branch, $baseDir) {
                $this->assertEquals($repo, $model->getProject());
                $this->assertEquals($branch, $model->getBranch());
                $this->assertEquals("$baseDir/repo/branch", $model->getRoot());
            })
        ;

        $webhookService = new WebhookService(
            $baseDir,
            $this->createMock(FileSystem::class),
            $manager,
            $this->createMock(Git::class),
            $repository,
            $this->createMock(WebhookTransformer::class),
        );

        $webhookService->create($repo, $branch);
    }

    public function testCreateFailAlreadyExists()
    {
        $repo = 'repo';
        $branch = 'branch';

        $repository = $this->createMock(WebhookRepository::class);
        $repository
            ->expects(self::once())
            ->method('findOneBy')
            ->with([
                'branch' => $branch,
                'project' => $repo,
            ])
            ->willReturn(new Webhook())
        ;

        $manager = $this->createMock(ProjectManager::class);
        $manager
            ->expects(self::never())
            ->method('update')
        ;

        $this->expectException(WebhookHandlerException::class);
        $this->expectExceptionMessage(sprintf('Webhook %s %s already exists', $repo, $branch));

        $webhookService = new WebhookService(
            'foo',
            $this->createMock(FileSystem::class),
            $manager,
            $this->createMock(Git::class),
            $repository,
            $this->createMock(WebhookTransformer::class),
        );

        $webhookService->create($repo, $branch);
    }

    public function testDeleteSuccess()
    {
        $repo = 'repo';
        $branch = 'branch';
        $baseDir = 'foo';

        $webhook = new Webhook();
        $webhook
            ->setBranch($branch)
            ->setProject($repo)
        ;

        $repository = $this->createMock(WebhookRepository::class);
        $repository
            ->expects(self::once())
            ->method('findOneBy')
            ->with([
                'branch' => $branch,
                'project' => $repo,
            ])
            ->willReturn($webhook)
        ;

        $hook = new HookModel($webhook->getProject(), $webhook->getBranch());
        $hook->setRoot($baseDir);

        $transformer = $this->createMock(WebhookTransformer::class);
        $transformer
            ->expects(self::once())
            ->method('toModel')
            ->with($webhook)
            ->willReturn($hook)
        ;

        $manager = $this->createMock(ProjectManager::class);
        $manager
            ->expects(self::once())
            ->method('remove')
            ->willReturnCallback(function (HookModel $model) use ($repo, $branch, $baseDir) {
                $this->assertEquals($repo, $model->getProject());
                $this->assertEquals($branch, $model->getBranch());
                $this->assertEquals("$baseDir/repo/branch", $model->getRoot());
            })
        ;

        $webhookService = new WebhookService(
            $baseDir,
            $this->createMock(FileSystem::class),
            $manager,
            $this->createMock(Git::class),
            $repository,
            $transformer,
        );

        $webhookService->delete($repo, $branch);
    }

    public function testDeleteFailNoRecord()
    {
        $repo = 'repo';
        $branch = 'branch';

        $repository = $this->createMock(WebhookRepository::class);
        $repository
            ->expects(self::once())
            ->method('findOneBy')
            ->with([
                'branch' => $branch,
                'project' => $repo,
            ])
            ->willReturn(null)
        ;

        $manager = $this->createMock(ProjectManager::class);
        $manager
            ->expects(self::never())
            ->method('remove')
        ;

        $this->expectException(WebhookHandlerException::class);
        $this->expectExceptionMessage(sprintf('Webhook not found %s %s', $repo, $branch));

        $transformer = $this->createMock(WebhookTransformer::class);
        $transformer
            ->expects(self::never())
            ->method('toModel')
        ;

        $webhookService = new WebhookService(
            'foo',
            $this->createMock(FileSystem::class),
            $manager,
            $this->createMock(Git::class),
            $repository,
            $transformer,
        );

        $webhookService->delete($repo, $branch);
    }

    public function testRecreateSuccess()
    {
        $repo = 'repo';
        $branch = 'branch';
        $baseDir = 'foo';

        $webhook = new Webhook();
        $webhook
            ->setBranch($branch)
            ->setProject($repo)
        ;

        $repository = $this->createMock(WebhookRepository::class);
        $repository
            ->expects(self::at(0))
            ->method('findOneBy')
            ->with([
                'branch' => $branch,
                'project' => $repo,
            ])
            ->willReturn($webhook)
        ;
        $repository
            ->expects(self::at(1))
            ->method('findOneBy')
            ->with([
                'branch' => $branch,
                'project' => $repo,
            ])
            ->willReturn(null)
        ;

        $hook = new HookModel($webhook->getProject(), $webhook->getBranch());
        $hook->setRoot($baseDir);

        $transformer = $this->createMock(WebhookTransformer::class);
        $transformer
            ->expects(self::once())
            ->method('toModel')
            ->with($webhook)
            ->willReturn($hook)
        ;

        $manager = $this->createMock(ProjectManager::class);
        $manager
            ->expects(self::once())
            ->method('update')
            ->willReturnCallback(function (HookModel $model) use ($repo, $branch, $baseDir) {
                $this->assertEquals($repo, $model->getProject());
                $this->assertEquals($branch, $model->getBranch());
                $this->assertEquals("$baseDir/repo/branch", $model->getRoot());
            })
        ;
        $manager
            ->expects(self::once())
            ->method('remove')
            ->willReturnCallback(function (HookModel $model) use ($repo, $branch, $baseDir) {
                $this->assertEquals($repo, $model->getProject());
                $this->assertEquals($branch, $model->getBranch());
                $this->assertEquals("$baseDir/repo/branch", $model->getRoot());
            })
        ;

        $webhookService = new WebhookService(
            $baseDir,
            $this->createMock(FileSystem::class),
            $manager,
            $this->createMock(Git::class),
            $repository,
            $transformer,
        );

        $webhookService->recreate($repo, $branch);
    }
}
