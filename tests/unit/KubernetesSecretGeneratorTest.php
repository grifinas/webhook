<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Model\ExecResponseModel;
use App\Service\KubernetesSecretGenerator;
use App\Service\ShellExecutor;
use App\Service\SslProvider;
use Codeception\Test\Unit;

class KubernetesSecretGeneratorTest extends Unit
{
    public function testIdRsa(): void
    {
        $secretGenerator = $this->makeService(static function ($command, $arguments) {
            self::assertEquals(
                'microk8s.kubectl create secret generic id-rsa -n $namespace --from-file=id_rsa=$privatePath --from-file=id_rsa.pub=$publicPath --from-file=known_hosts=$knownHostsPath',
                $command
            );
            self::assertEquals([
                'privatePath' => 'private',
                'publicPath' => 'public',
                'knownHostsPath' => 'hosts',
                'namespace' => 'namespace',
            ], $arguments);

            return new ExecResponseModel(0, []);
        });

        $secretGenerator->idRsa('namespace');
    }

    public function testSsl(): void
    {
        $secretGenerator = $this->makeService(static function ($command, $arguments) {
            self::assertEquals('microk8s.kubectl -n $namespace create secret generic ssl --from-file=$0', $command);
            self::assertEquals([
                '0' => 'ssl_file_1',
                'namespace' => 'namespace',
            ], $arguments);

            return new ExecResponseModel(0, []);
        });

        $secretGenerator->ssl('namespace');
    }

    private function makeService(callable $exec): KubernetesSecretGenerator
    {
        $executor = $this->makeEmpty(ShellExecutor::class, [
            'exec' => $exec
        ]);

        $ssl = $this->makeEmpty(SslProvider::class, [
            'get' => [
                'ssl_file_1'
            ]
        ]);

        return new KubernetesSecretGenerator('private', 'public', 'hosts', $executor, $ssl);
    }
}
