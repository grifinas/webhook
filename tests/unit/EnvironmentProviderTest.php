<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Exception\StrategyNotFoundException;
use App\Model\HookModel;
use App\Service\FileSystem;
use App\Strategy\Environment\K8sApacheEnvStrategy;
use App\Strategy\Environment\SS2SpaEnvStrategy;
use App\Strategy\EnvironmentProvider;
use Codeception\Test\Unit;

class EnvironmentProviderTest extends Unit
{
    private const REPOSITORY_SS2_SPA = 'shipment-system-spa';

    public function testGetFromListSuccess()
    {
        $model = (new HookModel(self::REPOSITORY_SS2_SPA, 'branch'))->setRoot('foo/project/branch');

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::never())
            ->method('exists')
        ;

        $provider = new EnvironmentProvider(
            'foo',
            $this->createMock(K8sApacheEnvStrategy::class),
            $this->createMock(SS2SpaEnvStrategy::class),
            $fs,
        );

        $provider->get($model);
    }

    public function testGetByK8s()
    {
        $model = (new HookModel('project', 'branch'))->setRoot('foo/project/branch');

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::once())
            ->method('exists')
            ->with("{$model->getRoot()}/k8s")
            ->willReturn(true)
        ;

        $provider = new EnvironmentProvider(
            'foo',
            $this->createMock(K8sApacheEnvStrategy::class),
            $this->createMock(SS2SpaEnvStrategy::class),
            $fs,
        );

        $provider->get($model);
    }

    public function testGetFail()
    {
        $model = (new HookModel('project', 'branch'))->setRoot('foo/project/branch');

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::once())
            ->method('exists')
            ->with("{$model->getRoot()}/k8s")
            ->willReturn(false)
        ;

        $provider = new EnvironmentProvider(
            'foo',
            $this->createMock(K8sApacheEnvStrategy::class),
            $this->createMock(SS2SpaEnvStrategy::class),
            $fs,
        );

        $this->expectException(StrategyNotFoundException::class);

        $provider->get($model);
    }
}
