<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Exception\StrategyNotFoundException;
use App\Model\HookModel;
use App\Service\FileSystem;
use App\Strategy\Update\K8SUpdateStrategy;
use App\Strategy\UpdateStrategyProvider;
use Codeception\Test\Unit;

class UpdateStrategyProviderTest extends Unit
{
    public function testGetSuccess()
    {
        $hook = (new HookModel('project', 'branch'))->setRoot('foo');

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::once())
            ->method('isDir')
            ->with("{$hook->getRoot()}/k8s")
            ->willReturn(true)
        ;

        $provider = new UpdateStrategyProvider(
            $fs,
            $this->createMock(K8SUpdateStrategy::class),
        );

        $provider->get($hook);
    }

    public function testGetFail()
    {
        $hook = (new HookModel('project', 'branch'))->setRoot('foo');

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::once())
            ->method('isDir')
            ->with("{$hook->getRoot()}/k8s")
            ->willReturn(false)
        ;

        $this->expectException(StrategyNotFoundException::class);

        $provider = new UpdateStrategyProvider(
            $fs,
            $this->createMock(K8SUpdateStrategy::class),
        );

        $provider->get($hook);
    }
}
