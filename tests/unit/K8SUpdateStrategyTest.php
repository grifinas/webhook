<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Exception\PodStateException;
use App\Model\HookModel;
use App\Model\KubernetesPodModel;
use App\Service\Kubernetes;
use App\Service\KubernetesFileSystem;
use App\Strategy\Update\K8SUpdateStrategy;
use Codeception\Test\Unit;

class K8SUpdateStrategyTest extends Unit
{
    public function testUpdateSuccess()
    {
        $name = 'name';

        $hook = new HookModel('project', 'branch');
        $hook->setName($name);

        $pod = new KubernetesPodModel($name, 'Running', 'namespace');

        $kubeFs = $this->createMock(KubernetesFileSystem::class);
        $kubeFs
            ->expects(self::once())
            ->method('getProjectName')
            ->with($hook)
            ->willReturn($name)
        ;

        $kube = $this->createMock(Kubernetes::class);
        $kube
            ->expects(self::once())
            ->method('findPod')
            ->with($name, $hook->getBranch())
            ->willReturn($pod)
        ;
        $kube
            ->expects(self::once())
            ->method('runMigrations')
            ->with($pod)
        ;

        $strategy = new K8SUpdateStrategy(
            $kube,
            $kubeFs,
        );

        $strategy->update($hook);
    }

    public function testUpdateFalse()
    {
        $name = 'name';

        $hook = new HookModel('project', 'branch');
        $hook->setName($name);

        $pod = new KubernetesPodModel($name, 'Crashed', 'namespace');

        $kubeFs = $this->createMock(KubernetesFileSystem::class);
        $kubeFs
            ->expects(self::once())
            ->method('getProjectName')
            ->with($hook)
            ->willReturn($name)
        ;

        $kube = $this->createMock(Kubernetes::class);
        $kube
            ->expects(self::once())
            ->method('findPod')
            ->with($name, $hook->getBranch())
            ->willReturn($pod)
        ;

        $this->expectException(PodStateException::class);
        $this->expectExceptionMessage('Pod is not found or not running');

        $strategy = new K8SUpdateStrategy(
            $kube,
            $kubeFs,
        );

        $strategy->update($hook);
    }
}
