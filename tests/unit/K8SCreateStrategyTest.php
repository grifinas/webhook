<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Model\HookModel;
use App\Service\Docker;
use App\Service\FileSystem;
use App\Service\Kubernetes;
use App\Service\KubernetesFileSystem;
use App\Service\KubernetesSecretGenerator;
use App\Strategy\Create\K8SCreateStrategy;
use Codeception\Test\Unit;
use PHPUnit\Framework\MockObject\MockObject;

class K8SCreateStrategyTest extends Unit
{
    private const CONFIG_DIRS = [
        'k8s/dev',
    ];
    private const YAML_CONFIG_ORDER = [
        'secrets',
        'deployment',
        'services',
        'ingress',
    ];

    private const DEV_NAMESPACE = 'namespace: venipak';

    public function testCreate()
    {
        $name = 'name';
        $link = 'link';
        $configs = ['conf-file_00', 'conf-file_01', 'conf-file_02', 'conf-file_03'];

        $hook = (new HookModel('project', 'branch'))
            ->setRoot('foo/project/branch')
            ->setName($name)
            ->setLink($link)
        ;

        $kubeFs = $this->createMock(KubernetesFileSystem::class);
        $kubeFs
            ->expects(self::once())
            ->method('copySamplesToYaml')
            ->with($hook, self::DEV_NAMESPACE, self::CONFIG_DIRS)
        ;
        $kubeFs
            ->expects(self::once())
            ->method('getProjectName')
            ->with($hook)
            ->willReturn($name)
        ;
        $kubeFs
            ->expects(self::once())
            ->method('getLink')
            ->with($hook)
            ->willReturn($link)
        ;
        $kubeFs
            ->expects(self::once())
            ->method('getConfigs')
            ->with($hook->getRoot(), self::YAML_CONFIG_ORDER, self::CONFIG_DIRS)
            ->willReturn($configs)
        ;

        $kubeFs
            ->expects(self::atLeastOnce())
            ->method('makeYaml')
            ->willReturnCallback(function (HookModel $model, string $config) use ($hook, $configs) {
                $this->assertTrue(in_array($config, $configs));
                $this->assertEquals($hook, $model);
            })
        ;

        $secret = $this->createMock(KubernetesSecretGenerator::class);
        $secret
            ->expects(self::once())
            ->method('idRsa')
            ->with($hook->getBranch())
            ->willReturnSelf()
        ;
        $secret
            ->expects(self::once())
            ->method('ssl')
            ->with($hook->getBranch())
            ->willReturnSelf()
        ;

        $kube = $this->createMock(Kubernetes::class);
        $kube
            ->expects(self::once())
            ->method('createNamespace')
            ->with($hook->getBranch())
        ;
        $kube
            ->expects(self::once())
            ->method('generateSecret')
            ->willReturn($secret)
        ;
        $kube
            ->expects(self::atLeastOnce())
            ->method('apply')
            ->willReturnCallback(function (string $config) use ($configs) {
                $this->assertTrue(in_array($config, $configs));
            })
        ;

        $docker = $this->createMock(Docker::class);
        $fs = $this->createMock(FileSystem::class);

        $this->buildImage($hook->getRoot(), $hook->getName(), $fs, $docker);

        $strategy = new K8SCreateStrategy(
            $kube,
            $docker,
            $fs,
            $kubeFs,
        );

        $model = $strategy->create($hook);
        $this->assertInstanceOf(HookModel::class, $model);
        $this->assertEquals($hook, $model);
    }

    private function buildImage(string $root, string $name, MockObject $fs, MockObject $docker)
    {
        $dockerfilePath = "$root/docker/Dockerfile";

        $fs
            ->expects(self::once())
            ->method('get')
            ->with($dockerfilePath)
            ->willReturn('foo')
        ;
        $fs
            ->expects(self::once())
            ->method('put')
            ->willReturnCallback(function (string $dockerfilePath, string $dockerfile) {
                $this->assertEquals('foo/project/branch/docker/Dockerfile', $dockerfilePath);
                $this->assertEquals("foo\nRUN echo '. /var/www/html/.env.local >> /etc/apache2/envvars'", $dockerfile);
                return 1;
            })
        ;

        $docker
            ->expects(self::once())
            ->method('build')
            ->with("$name:dev", $dockerfilePath, $root, [
                'APP_ENV' => 'dev',
                'RUN_COMPOSER' => 'yes',
            ], 'dev')
        ;
        $docker
            ->expects(self::once())
            ->method('push')
            ->with("$name:dev")
        ;
    }
}
