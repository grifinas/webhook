<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Exception\ValidationException;
use App\Model\EventModel;
use App\Service\RequestDeserializer;
use Codeception\Test\Unit;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestDeserializerTest extends Unit
{
    private const FQN = EventModel::class;

    public function testDeserialization()
    {
        $deserializer = $this->makeService(0);

        $model = $deserializer->deserialize(new Request(), self::FQN);
        self::assertInstanceOf(self::FQN, $model);
    }

    public function testValidationErrors()
    {
        $deserializer = $this->makeService(1);

        $this->expectException(ValidationException::class);
        $deserializer->deserialize(new Request(), self::FQN);
    }
    
    private function makeService(int $errorCount): RequestDeserializer
    {
        $serializer = $this->createMock(SerializerInterface::class);
        $serializer
            ->expects(self::once())
            ->method('deserialize')
            ->willReturnCallback(static function (string $data, string $fqn, string $type) {
                self::assertEquals(self::FQN, $fqn);
                self::assertEquals('json', $type);

                return new EventModel();
            })
        ;

        $constraints = $this->createMock(ConstraintViolationListInterface::class);
        $constraints
            ->expects($errorCount === 0 ? self::once() : self::exactly(2))
            ->method('count')
            ->willReturn($errorCount)
        ;

        $validator = $this->createMock(ValidatorInterface::class);
        $validator
            ->expects(self::once())
            ->method('validate')
            ->willReturn($constraints)
        ;

        return new RequestDeserializer($serializer, $validator);
    }
}
