<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Exception\StrategyNotFoundException;
use App\Model\HookModel;
use App\Service\FileSystem;
use App\Strategy\Delete\K8SDeleteStrategy;
use App\Strategy\DeleteStrategyProvider;
use Codeception\Test\Unit;

class DeleteStrategyProviderTest extends Unit
{
    public function testGetSuccess()
    {
        $hook = (new HookModel('project', 'branch'))->setRoot('foo');

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::once())
            ->method('isDir')
            ->with("{$hook->getRoot()}/k8s")
            ->willReturn(true)
        ;

        $provider = new DeleteStrategyProvider(
            $fs,
            $this->createMock(K8SDeleteStrategy::class),
        );

        $provider->get($hook);
    }

    public function testGetFail()
    {
        $hook = (new HookModel('project', 'branch'))->setRoot('foo');

        $fs = $this->createMock(FileSystem::class);
        $fs
            ->expects(self::once())
            ->method('isDir')
            ->with("{$hook->getRoot()}/k8s")
            ->willReturn(false)
        ;

        $this->expectException(StrategyNotFoundException::class);
        $this->expectExceptionMessage($hook->getProject());

        $provider = new DeleteStrategyProvider(
            $fs,
            $this->createMock(K8SDeleteStrategy::class),
        );

        $provider->get($hook);
    }
}
