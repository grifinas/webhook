<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Model\ExecResponseModel;
use App\Model\KubernetesPodModel;
use App\Service\Kubernetes;
use App\Service\KubernetesSecretGenerator;
use App\Service\ShellExecutor;
use Codeception\Test\Unit;

class KubernetesTest extends Unit
{
    public function testApply(): void
    {
        $kubernetes = $this->makeService(static function ($command, $arguments) {
            self::assertEquals('microk8s.kubectl apply -f $file', $command);
            self::assertEquals([
                'file' => 'foo',
            ], $arguments);

            return new ExecResponseModel(0, []);
        });

        $kubernetes->apply('foo');
    }

    public function testDelete(): void
    {
        $kubernetes = $this->makeService(static function ($command, $arguments) {
            self::assertEquals('microk8s.kubectl delete -f $file', $command);
            self::assertEquals([
                'file' => 'foo',
            ], $arguments);

            return new ExecResponseModel(0, []);
        });

        $kubernetes->delete('foo');
    }

    public function testDeleteNamespace(): void
    {
        $kubernetes = $this->makeService(static function ($command, $arguments) {
            self::assertEquals('microk8s.kubectl delete namespace $namespace', $command);
            self::assertEquals([
                'namespace' => 'foo',
            ], $arguments);

            return new ExecResponseModel(0, []);
        });

        $kubernetes->deleteNamespace('foo');
    }

    public function testGetPodByNamespace(): void
    {
        $kubernetes = $this->makeService(static function ($command, $arguments) {
            self::assertEquals('microk8s.kubectl get pods -n $namespace --no-headers=true', $command);
            self::assertEquals([
                'namespace' => 'foo',
            ], $arguments);

            return new ExecResponseModel(0, []);
        });

        $kubernetes->getNamespacePods('foo');
    }

    public function testCreateNamespace(): void
    {
        $kubernetes = $this->makeService(static function ($command, $arguments) {
            self::assertEquals('microk8s.kubectl create namespace $namespace', $command);
            self::assertEquals([
                'namespace' => 'foo',
            ], $arguments);

            return new ExecResponseModel(0, []);
        });

        $kubernetes->createNamespace('foo');
    }

    public function testFindPod(): void
    {
        $kubernetes = $this->makeService(static function ($command, $arguments) {
            self::assertEquals('microk8s.kubectl -n $namespace get pods', $command);
            self::assertEquals([
                'namespace' => 'foo',
            ], $arguments);

            return new ExecResponseModel(0, [
                'NAME                           READY   STATUS                       RESTARTS   AGE     IP          NODE   NOMINATED NODE   READINESS GATES',
                'oidc-64db9997b9-zp6fl          2/2     Running                      2          7d6h    10.1.1.60   pc     <none>           <none>',
                'rabbitmq-0                     0/1     CreateContainerConfigError   2          19d     10.1.1.63   pc     <none>           <none>',
                'rabbitmq-5b4f88ccb5-6vgk4      1/1     Running                      1          5d2h    10.1.1.62   pc     <none>           <none>',
                'ss2-api-656494977c-hzjlr       3/3     Running                      7          3d23h   10.1.1.57   pc     <none>           <none>',
                'ss2-spa-7b8c5c7856-fljph       1/1     Running                      1          7d6h    10.1.1.61   pc     <none>           <none>',
                'ups-adapter-6974bb7659-7z5qv   2/2     Running                      16         11d     10.1.1.65   pc     <none>           <none>',
            ]);
        });

        $pod = $kubernetes->findPod('ss2-api', 'foo');
        self::assertEquals('ss2-api-656494977c-hzjlr', $pod->name);
        self::assertEquals(KubernetesPodModel::STATUS_RUNNING, $pod->status);
        self::assertEquals('foo', $pod->namespace);
    }

    public function testGetContainerLogs(): void
    {
        $kubernetes = $this->makeService(static function ($command, $arguments) {
            self::assertEquals('microk8s.kubectl logs -n $namespace $podName $containerName', $command);
            self::assertEquals([
                'namespace' => 'foo',
                'podName' => 'name',
                'containerName' => 'api',
            ], $arguments);

            return new ExecResponseModel(0, [
                'log line 0',
                'log line 1',
            ]);
        });

        $pod = new KubernetesPodModel('name', 'Running', 'foo');

        $result = $kubernetes->getContainerLog($pod, 'api');

        $this->assertEquals('log line 0', $result[0]);
        $this->assertEquals('log line 1', $result[1]);
    }

    public function testRunMigrations(): void
    {
        $kubernetes = $this->makeService(static function ($command, $arguments) {
            self::assertEquals('microk8s.kubectl -n $namespace exec $pod -- bin/console doctrine:migrations:migrate --allow-no-migration -n', $command);
            self::assertEquals([
                'namespace' => 'namespace',
                'pod' => 'name',
            ], $arguments);

            return new ExecResponseModel(0, []);
        });

        $pod = new KubernetesPodModel('name', KubernetesPodModel::STATUS_RUNNING, 'namespace');

        $kubernetes->runMigrations($pod);
    }

    private function makeService(callable $exec): Kubernetes
    {
        $executor = $this->makeEmpty(ShellExecutor::class, [
            'exec' => $exec
        ]);

        return new Kubernetes($executor, $this->makeEmpty(KubernetesSecretGenerator::class));
    }
}
