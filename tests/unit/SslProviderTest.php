<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Service\FileSystem;
use App\Service\ShellExecutor;
use App\Service\SslProvider;
use Codeception\Test\Unit;
use Exception;

class SslProviderTest extends Unit
{
    public function testGetReturnsFilesWithoutDoingMuch(): void
    {
        $provider = $this->makeService(static function () {
            throw new Exception('shouldnt have come here');
        });

        $files = $provider->get();

        $expect = array_map(fn (string $file) => "dir/$file", SslProvider::FILES_REQUIRED);

        self::assertEquals($expect, $files);
    }

    public function testGetGeneratesFilesIfMissing(): void
    {
        $provider = $this->makeService(static function (string $command, $args) {
            if ($command === 'rm -rf $dir') {
                self::assertEquals([
                    'dir' => 'dir/*'
                ], $args);
            } elseif ($command === 'mkdir -p $sslDir && cd $sslDir && openssl genrsa -out rootCA.key 4096 && openssl req -x509 -new -nodes -key rootCA.key -sha256 -days 3650 -out $rootCA -subj "/C=LT/ST=Vilnius/L=EN/O=Megodata/OU=venipak/CN=*.venipak.megodata" && openssl genrsa -out $wildcardKey 2048 && openssl req -new -sha256 -key $wildcardKey -subj "/C=LT/ST=Vilnius/O=Megodata/CN=*.venipak.svc.cluster.local" -out wildcard.venipak.csr && openssl x509 -req -in wildcard.venipak.csr -CA $rootCA -CAkey rootCA.key -CAcreateserial -out $wildcardCrt -days 1024 -sha256') {
                self::assertEquals('dir', $args['sslDir']);
            } else {
                throw new Exception('shouldnt have come here');
            }
        }, true);

        $files = $provider->get();

        $expect = array_map(fn (string $file) => "dir/$file", SslProvider::FILES_REQUIRED);

        self::assertEquals($expect, $files);
    }

    private function makeService(callable $exec, bool $exists = true): SslProvider
    {
        $executor = $this->makeEmpty(ShellExecutor::class, [
            'exec' => $exec
        ]);

        $fileSystem = $this->makeEmpty(FileSystem::class, [
            'fileExists' => $exists
        ]);

        return new SslProvider('dir', $executor, $fileSystem);
    }
}
