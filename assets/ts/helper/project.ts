export const translateProjectName = (name: string) => {
  const map: {[index: string]: string} = {
    'shipment-system-api': 'ss2-api',
    'shipment-system-spa': 'ss2-spa',
  };

  return map[name] || name;
}
