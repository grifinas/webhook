interface LogContext {
  command: string;
  sanitized: string;
}

export interface WebhookLog {
  message: string;
  context: LogContext;
  datetime: string;
}

export interface PodLogs {
  [key: string]: string[];
}

export interface PodLogsRequest {
  project: string;
  repository: string;
  branch: string;
}
