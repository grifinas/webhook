export type Projects = 'ss2-api' | 'ss2-spa' | 'oidc' | 'ups-adapter';

export interface HookInterface {
  id: number;
  branch: string;
  project: Projects;
  name: string;
  link: string;
  createdAt: string;
}
