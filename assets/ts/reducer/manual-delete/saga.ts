import { ReduxAction } from '../../service/helpers';
import Fetch from '../../service/fetch';
import { put, takeEvery } from 'redux-saga/effects';
import * as actions from './actions';
import * as hookActions from '../hook/actions';

function* manualDeleteBranches(action: ReduxAction<actions.ManualDeleteRequest>) {
  try {
    yield new Fetch().post(`/manual-delete/${action.payload.project}/${action.payload.branch}`);
    yield put(hookActions.getHooks.request.execute());
    yield put(actions.manualDelete.success.execute(action.payload));
  } catch (e) {
    yield put(actions.manualDelete.error.execute(action.payload));
  }
}

export const manualDeleteSaga = [takeEvery(actions.manualDelete.request.type, manualDeleteBranches)];
