import { createReducer, Dictionary } from '../../service/helpers';
import { manualDelete, ManualDeleteError, ManualDeleteRequest, ManualDeleteSuccess } from './actions';

export interface ManualDeleteState {
  isDeleting: Dictionary<boolean>;
}

const defaultState: ManualDeleteState = { isDeleting: {} };
export const manualDeleteReducer = createReducer(defaultState, {
  [manualDelete.request.type]: (draft, payload: ManualDeleteRequest) => {
    draft.isDeleting[`${payload.project}.${payload.branch}`] = true;
  },
  [manualDelete.success.type]: (draft, payload: ManualDeleteSuccess) => {
    draft.isDeleting[`${payload.project}.${payload.branch}`] = false;
  },
  [manualDelete.error.type]: (draft, payload: ManualDeleteError) => {
    draft.isDeleting[`${payload.project}.${payload.branch}`] = false;
  },
});
