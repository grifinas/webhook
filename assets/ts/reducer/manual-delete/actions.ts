import { createAsyncAction } from '../../service/helpers';

interface Base {
  project: string;
  branch: string;
}

export interface ManualDeleteRequest extends Base { }
export interface ManualDeleteSuccess extends Base { }
export interface ManualDeleteError extends Base { }

export const manualDelete =
  createAsyncAction<ManualDeleteRequest, ManualDeleteSuccess, ManualDeleteError>('manualDelete');
