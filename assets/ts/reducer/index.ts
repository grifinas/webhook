import { combineReducers } from 'redux';
import { all } from 'redux-saga/effects';
import { branchesReducer, BranchesState } from './hook/reducer';
import { webhookLogsReducer, WebhookLogState } from './webhook-log/reducer';
import { podLogsReducer, PodLogsState } from './pod-logs/reducer';
import { projectBranchesReducer, ProjectBranchState } from './project-branches/reducer';
import { projectsReducer, ProjectsState } from './projects/reducer';
import { hookSaga } from './hook/saga';
import { webhookLogSaga } from './webhook-log/saga';
import { podLogsSaga } from './pod-logs/saga';
import { projectBranchesSaga } from './project-branches/saga';
import { projectsSaga } from './projects/saga';
import { manualCreateReducer, ManualCreateState } from './manual-create/reducer';
import { manualCreateSaga } from './manual-create/saga';
import { manualDeleteReducer, ManualDeleteState } from './manual-delete/reducer';
import { manualDeleteSaga } from './manual-delete/saga';
import { manualRecreateReducer, ManualRecreateState } from './manual-recreate/reducer';
import { manualRecreateSaga } from './manual-recreate/saga';
import { environmentReducer, EnvironmentState } from './environment/reducer';
import { environmentSaga } from './environment/saga';

export interface AppState {
  branches: BranchesState;
  webhookLogs: WebhookLogState;
  podLogs: PodLogsState;
  projects: ProjectsState;
  projectBranches: ProjectBranchState;
  manualCreate: ManualCreateState;
  manualDelete: ManualDeleteState;
  manualRecreate: ManualRecreateState;
  environment: EnvironmentState;
}

export const reducers = combineReducers<any>({
  branches: branchesReducer,
  webhookLogs: webhookLogsReducer,
  podLogs: podLogsReducer,
  projects: projectsReducer,
  projectBranches: projectBranchesReducer,
  manualCreate: manualCreateReducer,
  manualDelete: manualDeleteReducer,
  manualRecreate: manualRecreateReducer,
  environment: environmentReducer,
});

export default function* sagas() {
  yield all([
    ...hookSaga,
    ...webhookLogSaga,
    ...podLogsSaga,
    ...projectsSaga,
    ...projectBranchesSaga,
    ...manualCreateSaga,
    ...manualDeleteSaga,
    ...manualRecreateSaga,
    ...environmentSaga,
  ]);
}
