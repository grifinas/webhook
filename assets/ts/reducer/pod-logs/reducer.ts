import { createReducer } from '../../service/helpers';
import { PodLogs } from '../../model/logs';
import { loadPodLogs, PodLogsSuccess } from './actions';

export interface PodLogsState {
  logs: PodLogs;
}

const defaultPodLogsState: PodLogsState = { logs: {} };
export const podLogsReducer = createReducer(defaultPodLogsState, {
  [loadPodLogs.success.type]: (draft, payload: PodLogsSuccess) => {
    draft.logs = payload.logs;
  },
});
