import { ReduxAction } from '../../service/helpers';
import Fetch from '../../service/fetch';
import { put, takeEvery } from 'redux-saga/effects';
import * as actions from './actions';
import { PodLogs } from '../../model/logs';

function* loadPodLogs(action: ReduxAction<actions.LoadPodLogs>) {
  const logs: PodLogs = yield new Fetch().post<PodLogs>('/hook-log', action.payload.request);
  yield put(actions.loadPodLogs.success.execute({ logs }));
}

export const podLogsSaga = [takeEvery(actions.loadPodLogs.request.type, loadPodLogs)];
