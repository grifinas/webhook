import { createAsyncAction } from '../../service/helpers';
import { PodLogs, PodLogsRequest } from '../../model/logs';

export interface PodLogsSuccess {
  logs: PodLogs;
}

export interface LoadPodLogs {
  request: PodLogsRequest;
}

export const loadPodLogs = createAsyncAction<LoadPodLogs, PodLogsSuccess>('loadPodLogs');
