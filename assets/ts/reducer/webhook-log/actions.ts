import { WebhookLog } from '../../model/logs';
import { createAsyncAction } from '../../service/helpers';

export interface SetWebhookLogsPayload {
  logs: WebhookLog[];
}

export const loadWebhookLogs = createAsyncAction<void, SetWebhookLogsPayload>('loadWebhookLogs');
