import { ReduxAction } from '../../service/helpers';
import { WebhookLog } from '../../model/logs';
import Fetch from '../../service/fetch';
import { put, takeEvery } from 'redux-saga/effects';
import * as actions from './actions';

function* getWebhookLog(action: ReduxAction<void>) {
  const logs: WebhookLog[] = yield new Fetch().get<WebhookLog[]>('/webhook-log');
  yield put(actions.loadWebhookLogs.success.execute({ logs }));
}

export const webhookLogSaga = [
  takeEvery(actions.loadWebhookLogs.request.type, getWebhookLog),
];
