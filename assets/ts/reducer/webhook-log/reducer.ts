import { WebhookLog } from '../../model/logs';
import { loadWebhookLogs, SetWebhookLogsPayload } from './actions';
import { createReducer } from '../../service/helpers';

export interface WebhookLogState {
  logs: WebhookLog[];
}

const defailtWebhookLogState: WebhookLogState = { logs: [] };
export const webhookLogsReducer = createReducer(defailtWebhookLogState, {
  [loadWebhookLogs.success.type]: (draft, payload: SetWebhookLogsPayload) => {
    draft.logs = payload.logs;
  },
});
