import { ReduxAction } from '../../service/helpers';
import { HookInterface } from '../../model/webhook';
import Fetch from '../../service/fetch';
import { put, takeEvery } from 'redux-saga/effects';
import * as actions from './actions';

function* getHooks(action: ReduxAction<void>) {
  const webhooks: HookInterface[] = yield new Fetch().get<HookInterface[]>('/webhooks');
  yield put(actions.getHooks.success.execute({
    hooks: webhooks.sort((a: HookInterface, b: HookInterface) => {
      return Date.parse(b.createdAt) - Date.parse(a.createdAt);
    }),
  }));
}

export const hookSaga = [
  takeEvery(actions.getHooks.request.type, getHooks),
];
