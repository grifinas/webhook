import { HookInterface } from '../../model/webhook';
import { createReducer } from '../../service/helpers';
import { getHooks, SetHooksPayload } from './actions';

export interface BranchesState {
  hooks: HookInterface[];
}

const defaultHookState: BranchesState = { hooks: [] };
export const branchesReducer = createReducer(defaultHookState, {
  [getHooks.success.type]: (draft,  payload: SetHooksPayload) => {
    draft.hooks = payload.hooks;
  },
});
