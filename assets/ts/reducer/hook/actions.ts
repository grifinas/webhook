import { HookInterface } from '../../model/webhook';
import { createAsyncAction } from '../../service/helpers';

export interface SetHooksPayload {
  hooks: HookInterface[];
}

export const getHooks = createAsyncAction<void, SetHooksPayload>('getHooks');
