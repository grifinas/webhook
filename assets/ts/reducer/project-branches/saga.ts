import { ReduxAction } from '../../service/helpers';
import Fetch from '../../service/fetch';
import { put, takeEvery } from 'redux-saga/effects';
import * as actions from './actions';

function* loadProjectBranches(action: ReduxAction<actions.ProjectBranchesRequest>) {
  try {
    const branchNames: string[] = yield new Fetch().get<string[]>(`/get-branches/${action.payload.projectName}`);
    yield put(actions.loadProjectBranches.success.execute({ branchNames }));
  } catch (e) {
    yield put(actions.loadProjectBranches.error.execute());
  }
}

export const projectBranchesSaga = [takeEvery(actions.loadProjectBranches.request.type, loadProjectBranches)];
