import { createReducer } from '../../service/helpers';
import { loadProjectBranches, ProjectBranchesRequest, ProjectBranchesResponse } from './actions';

export interface ProjectBranchState {
  branchNames: string[];
}

const defaultState: ProjectBranchState = { branchNames: [] };
export const projectBranchesReducer = createReducer(defaultState, {
  [loadProjectBranches.request.type]: (draft) => {
    draft.branchNames = defaultState.branchNames;
  },
  [loadProjectBranches.success.type]: (draft, payload: ProjectBranchesResponse) => {
    draft.branchNames = payload.branchNames;
  },
  [loadProjectBranches.error.type]: (draft) => {
    draft.branchNames = ['master'];
  },
});
