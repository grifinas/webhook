import { createAsyncAction } from '../../service/helpers';

export interface ProjectBranchesResponse {
  branchNames: string[];
}

export interface ProjectBranchesRequest {
  projectName: string;
}

export const loadProjectBranches = createAsyncAction<ProjectBranchesRequest, ProjectBranchesResponse>('loadProjectBranches');
