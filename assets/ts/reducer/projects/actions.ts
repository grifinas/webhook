import { createAsyncAction } from '../../service/helpers';

export interface LoadProjectsResponse {
  projects: string[];
}

export const loadProjects = createAsyncAction<void, LoadProjectsResponse>('loadProjects');
