import Fetch from '../../service/fetch';
import { put, takeEvery } from 'redux-saga/effects';
import * as actions from './actions';

function* loadProjects() {
  const projects: string[] = yield new Fetch().get<string[]>('/get-projects');
  yield put(actions.loadProjects.success.execute({ projects }));
}

export const projectsSaga = [
  takeEvery(actions.loadProjects.request.type, loadProjects),
];
