import { WebhookLog } from '../../model/logs';
import { loadProjects, LoadProjectsResponse } from './actions';
import { createReducer } from '../../service/helpers';

export interface ProjectsState {
  projects: string[];
}

const defaultState: ProjectsState = { projects: [] };
export const projectsReducer = createReducer(defaultState, {
  [loadProjects.success.type]: (draft, payload: LoadProjectsResponse) => {
    draft.projects = payload.projects;
  },
});
