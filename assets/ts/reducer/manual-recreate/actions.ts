import { createAsyncAction } from '../../service/helpers';

interface Base {
  project: string;
  branch: string;
}

export interface ManualRecreateRequest extends Base { }
export interface ManualRecreateSuccess extends Base { }
export interface ManualRecreateError extends Base { }

export const manualRecreate =
  createAsyncAction<ManualRecreateRequest, ManualRecreateSuccess, ManualRecreateError>('manualRecreate');
