import { ReduxAction } from '../../service/helpers';
import Fetch from '../../service/fetch';
import { put, takeEvery } from 'redux-saga/effects';
import * as actions from './actions';
import * as hookActions from '../hook/actions';

function* manualRecreateBranches(action: ReduxAction<actions.ManualRecreateRequest>) {
  try {
    yield new Fetch().post(`/manual-recreate/${action.payload.project}/${action.payload.branch}`);
    yield put(hookActions.getHooks.request.execute());
    yield put(actions.manualRecreate.success.execute(action.payload));
  } catch (e) {
    yield put(actions.manualRecreate.error.execute(action.payload));
  }
}

export const manualRecreateSaga = [takeEvery(actions.manualRecreate.request.type, manualRecreateBranches)];
