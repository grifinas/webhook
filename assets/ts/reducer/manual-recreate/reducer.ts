import { createReducer, Dictionary } from '../../service/helpers';
import { manualRecreate, ManualRecreateError, ManualRecreateRequest, ManualRecreateSuccess } from './actions';

export interface ManualRecreateState {
  isRecreating: Dictionary<boolean>;
}

const defaultState: ManualRecreateState = { isRecreating: { } };
export const manualRecreateReducer = createReducer(defaultState, {
  [manualRecreate.request.type]: (draft, payload: ManualRecreateRequest) => {
    draft.isRecreating[`${payload.project}.${payload.branch}`] = true;
  },
  [manualRecreate.success.type]: (draft, payload: ManualRecreateSuccess) => {
    draft.isRecreating[`${payload.project}.${payload.branch}`] = false;
  },
  [manualRecreate.error.type]: (draft, payload: ManualRecreateError) => {
    draft.isRecreating[`${payload.project}.${payload.branch}`] = false;
  },
});
