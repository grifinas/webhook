import { createReducer, Dictionary } from '../../service/helpers';
import * as actions from './actions';

export interface EnvironmentState {
  env: Dictionary<actions.EnvResponse>;
  isSaving: boolean;
}

const defaultState: EnvironmentState = { env: {}, isSaving: false };
export const environmentReducer = createReducer(defaultState, {
  [actions.loadProjectEnvironment.success.type]: (draft, payload: actions.ProjectEnvironmentResponse) => {
    draft.env[payload.id] = payload.env;
  },
  [actions.loadProjectEnvironment.error.type]: (draft, payload: actions.ProjectEnvironmentError) => {
    draft.env[payload.id] = defaultState.env[payload.id];
  },
  [actions.saveProjectEnvironment.request.type]: (draft) => {
    draft.isSaving = true;
  },
  [actions.saveProjectEnvironment.success.type]: (draft) => {
    draft.isSaving = false;
  },
  [actions.saveProjectEnvironment.error.type]: (draft) => {
    draft.isSaving = false;
  },
});
