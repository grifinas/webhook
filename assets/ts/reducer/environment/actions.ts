import { createAsyncAction } from '../../service/helpers';

export interface ProjectEnvironmentRequest {
  id: number;
}

export interface ProjectEnvironmentResponse {
  id: number;
  env: EnvResponse;
}

export interface ProjectEnvironmentError {
  id: number;
}

export interface EnvDictionary {
  [index: string]: string;
}

export interface EnvResponse extends EnvDictionary {}

export interface ProjectEnvironmentPayload {
  id: number;
  env: EnvDictionary;
}

export const loadProjectEnvironment = createAsyncAction<ProjectEnvironmentRequest, ProjectEnvironmentResponse, ProjectEnvironmentError>('load_project_environment');
export const saveProjectEnvironment = createAsyncAction<ProjectEnvironmentPayload, any, any>('save_project_environment');

