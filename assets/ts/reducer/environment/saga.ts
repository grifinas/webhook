import { ReduxAction } from '../../service/helpers';
import Fetch from '../../service/fetch';
import { put, takeEvery } from 'redux-saga/effects';
import * as actions from './actions';

function* loadProjectEnvironment(action: ReduxAction<actions.ProjectEnvironmentRequest>) {
  try {
    const cunt = yield new Fetch().get<actions.EnvResponse>(`/webhook/${action.payload.id}/env`);
    yield put(actions.loadProjectEnvironment.success.execute({ env: cunt, id: action.payload.id }));
  } catch (e) {
    yield put(actions.loadProjectEnvironment.error.execute({ id: action.payload.id }));
  }
}

function* saveProjectEnvironment(action: ReduxAction<actions.ProjectEnvironmentPayload>) {
  try {
    yield new Fetch().post<never>(`/webhook/${action.payload.id}/env`, { environment: action.payload.env });
    yield put(actions.saveProjectEnvironment.success.execute({}));
  } catch (e) {
    yield put(actions.saveProjectEnvironment.error.execute({}));
  }
}

export const environmentSaga = [
  takeEvery(actions.loadProjectEnvironment.request.type, loadProjectEnvironment),
  takeEvery(actions.saveProjectEnvironment.request.type, saveProjectEnvironment),
];
