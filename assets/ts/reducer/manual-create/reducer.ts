import { createReducer } from '../../service/helpers';
import { manualCreate } from './actions';

export interface ManualCreateState {
  isCreating: boolean;
}

const defaultState: ManualCreateState = { isCreating: false };
export const manualCreateReducer = createReducer(defaultState, {
  [manualCreate.request.type]: (draft) => {
    draft.isCreating = true;
  },
  [manualCreate.success.type]: (draft) => {
    draft.isCreating = false;
  },
  [manualCreate.error.type]: (draft) => {
    draft.isCreating = false;
  },
});
