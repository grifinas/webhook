import { createAsyncAction } from '../../service/helpers';

export interface ManualCreateRequest {
  project: string;
  branch: string;
}

export const manualCreate = createAsyncAction<ManualCreateRequest, void>('manualCreate');
