import { ReduxAction } from '../../service/helpers';
import Fetch from '../../service/fetch';
import { put, takeEvery } from 'redux-saga/effects';
import * as actions from './actions';
import * as hookActions from '../hook/actions';

function* manualCreateBranches(action: ReduxAction<actions.ManualCreateRequest>) {
  try {
    yield new Fetch().post(`/manual-create/${action.payload.project}/${action.payload.branch}`);
    yield put(hookActions.getHooks.request.execute());
    yield put(actions.manualCreate.success.execute());
  } catch (e) {
    yield put(actions.manualCreate.error.execute());
  }
}

export const manualCreateSaga = [takeEvery(actions.manualCreate.request.type, manualCreateBranches)];
