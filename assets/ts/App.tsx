import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../css/app.scss';
import Search from './components/Search';
import ModalContainer from './components/ModalContainer';
import ActionMenu from './components/ActionMenu';
import Webhook from './components/Webhook';
import { HookInterface } from './model/webhook';
import { getHooks } from './reducer/hook/actions';
import { AppState } from './reducer';

interface State {
  hooks: HookInterface[];
}

interface ConnectedStateProps {
  hooks: HookInterface[];
}

interface DispatchProps {
  getHooks: () => void;
}

interface OwnProps {
}

interface Props extends ConnectedStateProps, DispatchProps, OwnProps {
}

class App extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      hooks: this.props.hooks,
    };
  }

  componentDidMount(): void {
    this.props.getHooks();
  }

  componentDidUpdate(prevProps: Props): void {
    if (this.props.hooks !== prevProps.hooks) {
      this.search();
    }
  }

  render() {
    return (
      <div className="App">
        <ModalContainer/>
        <div className="container lg mx-auto">
          <Search onChange={this.search}/>
          <ActionMenu/>
          <Webhook hooks={this.state.hooks}/>
        </div>
      </div>
    );
  }

  private search = (search: string = ''): void => {
    const { hooks } = this.props;

    let hooksList: HookInterface[] = [];

    if (!search) {
      hooksList = hooks;
    }

    if (search) {
      for (const idx in hooks) {
        const branch = hooks[idx].branch.toLowerCase();
        const project = hooks[idx].project.toLowerCase();
        const name = hooks[idx].name.toLowerCase();

        const key = search.toLowerCase();
        if (branch.includes(key) || project.includes(key) || name.includes(key)) {
          hooksList.push(hooks[idx]);
        }
      }
    }

    this.setState({ hooks: hooksList });
  }
}

export default connect<ConnectedStateProps, DispatchProps, OwnProps, AppState>(
  (state) => {
    return {
      hooks: state.branches.hooks,
    };
  },
  (dispatch, ownProps) => {
    return {
      getHooks: () => dispatch(getHooks.request.execute()),
    };
  })(App);
