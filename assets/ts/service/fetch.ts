import { Dictionary } from './helpers';
import { stringify } from 'querystring';
import * as HttpStatus from 'http-status-codes';

class Fetch {
  public get<T>(url: string, params: Dictionary<string> = {}): Promise<T> {
    const link = `${url}${url.indexOf('?') === -1 ? '?' : '&'}`;
    return this.call<T>(`${link}${stringify(params)}`);
  }

  public post<T>(url: string, data: any[] | object = {}): Promise<T> {
    return this.call<T>(url, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: { 'Content-Type': 'application/json' },
    });
  }

  private call<T>(url: string, options?: RequestInit): Promise<T> {

    return fetch(url, { ...options })
      .then((response: Response) => this.callback<T>(response))
      .catch((error: Error) => {
        console.log(error.message);
        throw error;
      });
  }

  private callback<T>(response: Response): Promise<T> {
    if (!response.ok) {
      console.log(response);
      throw new Error('Bad response from API');
    }

    if (response.status === HttpStatus.NO_CONTENT) {
      return {} as Promise<T>;
    }

    return response.clone().json() as Promise<T>;
  }
}

export default Fetch;
