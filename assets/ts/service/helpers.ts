import produce, { Draft } from 'immer';

export interface Dictionary<T> {
  [key: string]: T;
}

export function* objectIterator<T>(obj: Dictionary<T>): IterableIterator<[T, string]> {
  for (const i in obj) {
    if (obj.hasOwnProperty(i)) {
      yield [obj[i], i];
    }
  }
}

export interface ReduxAction<Payload> {
  type: string;
  payload: Payload;
}

function executeAction<Payload>(type: string, payload: Payload): ReduxAction<Payload> {
  return { type, payload };
}

type ReducerActionHandler<State, Payload> = (draft: State, payload: Payload) => void;
type ReducerActionHandlersDictionary<State, Payload> = Dictionary<ReducerActionHandler<State, Payload>>;

class ReducerActionsHandler<State> {
  private state: State;
  private action: ReduxAction<any>;
  private handlers: Function[];

  constructor(state: State, action: ReduxAction<any>, handlers: ReducerActionHandlersDictionary<Draft<State>, any>) {
    this.state = state;
    this.action = action;
    this.handlers = [];

    for (const [handler, type] of objectIterator(handlers)) {
      this.handlers.push(() => this.handleAction(type, this.state, this.action, handler));
    }
  }

  public execute() {
    this.handlers.forEach((handler) => {
      this.state = handler();
    });

    return this;
  }

  public getState() {
    return this.state;
  }

  private handleAction<State, Payload>(
    type: string,
    state: State,
    action: ReduxAction<Payload>,
    handler: ReducerActionHandler<Draft<State>, Payload>,
  ) {
    if (type !== action.type) {
      return state;
    }

    return produce(state, (draft) => {
      handler(draft, action.payload);
    });
  }
}

export function createAsyncAction<RequestPayload, SuccessPayload = RequestPayload, ErrorPayload = void>(type: string) {
  return {
    request: createAction<RequestPayload>(`${type}_request`),
    success: createAction<SuccessPayload>(`${type}_success`),
    error: createAction<ErrorPayload>(`${type}_error`),
  };
}

export function createAction<Payload>(type: string) {
  return {
    type,
    execute: (payload: Payload) => executeAction<Payload>(type, payload),
  };
}

export function createReducer<State>(
  defaultState: State,
  handlers: ReducerActionHandlersDictionary<Draft<State>, any>,
) {
  return (state: State, action: ReduxAction<any>) => {
    return (new ReducerActionsHandler(state || defaultState, action, handlers)).execute().getState();
  };
}

export function getActionTypeCreator(moduleName: string) {
  return (actionType: string) => `${moduleName}_${actionType}`;
}
