import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRedo, faFileAlt, faExternalLinkAlt, faTimes, faLeaf } from '@fortawesome/free-solid-svg-icons';
import { modal } from './ModalContainer';
import ModalHookLogs from './modals/ModalHookLogs';
import { HookInterface } from '../model/webhook';
import { connect } from 'react-redux';
import index, { AppState } from '../reducer';
import { manualDelete, ManualDeleteRequest } from '../reducer/manual-delete/actions';
import { manualRecreate, ManualRecreateRequest } from '../reducer/manual-recreate/actions';
import ModalEnv from './modals/ModalEnv';
import { translateProjectName } from '../helper/project';

interface ConnectedStateProps {
  isRecreating: boolean;
  isDeleting: boolean;
}

interface DispatchProps {
  manualDelete: (request: ManualDeleteRequest) => void;
  manualRecreate: (request: ManualRecreateRequest) => void;
}
interface OwnProps {
  hook: HookInterface;
}

interface Props extends DispatchProps, ConnectedStateProps, OwnProps { }

class Hook extends Component<Props> {

  private delete = () => {
    const { project, branch } = this.props.hook;
    this.props.manualDelete({ project, branch });
  }

  private recreate = () => {
    const { project, branch } = this.props.hook;
    this.props.manualRecreate({ project, branch });
  }

  render() {
    const { id, branch, link, name, project } = this.props.hook;
    const { isRecreating, isDeleting } = this.props;
    const disabled = isRecreating || isDeleting;

    return (
      <div
        className={`box w-full p-2 mb-2 shadow rounded bg-gray-10 hover-bg-gray-15 ${disabled ? 'hook-disabled' : ''}`}
      >
        <div className="w-1_12 inline-block">
          <div className={`tag ${project} mr-1 text-center`}>
            {translateProjectName(project)}
          </div>
        </div>
        <div className="w-9_12 inline-block">
          <div className="tag mx-2">
            <FontAwesomeIcon icon={faExternalLinkAlt} className="text-gray-30"/>
            <a
              href={`http://${link}`}
              target="_blank"
              rel="noopener noreferrer"
              className="ml-2 text-blue hover-text-red no-decorations"
            >
              {branch}
            </a>
          </div>
        </div>
        <div className="w-2_12 inline-block text-center">
          <div
            onClick={() => modal(<ModalHookLogs hook={this.props.hook}/>)}
            className="inline-block"
          >
            <div className="button">
              <FontAwesomeIcon icon={faFileAlt}/>
            </div>
          </div>

          <div
            onClick={() => modal(<ModalEnv id={id}/>)}
            className="inline-block"
          >
            <div className="button">
              <FontAwesomeIcon icon={faLeaf}/>
            </div>
          </div>

          <div
            onClick={this.recreate}
            className="inline-block"
          >
            <div className="button bg-yellow">
              <FontAwesomeIcon icon={faRedo}/>
            </div>
          </div>

          <div
            onClick={this.delete}
            className="inline-block"
          >
            <div className="button bg-red">
              <FontAwesomeIcon icon={faTimes}/>
            </div>
          </div>

        </div>
      </div>
    );
  }
}

export default connect<ConnectedStateProps, DispatchProps, OwnProps, AppState>(
  (state, props) => {
    const key = `${props.hook.project}.${props.hook.branch}`;
    return {
      isRecreating: state.manualRecreate.isRecreating[key] || false,
      isDeleting: state.manualDelete.isDeleting[key] || false,
    };
  },
  (dispatch, ownProps) => {
    return {
      manualDelete: (request: ManualDeleteRequest) => {
        dispatch(manualDelete.request.execute(request));
      },
      manualRecreate: (request: ManualRecreateRequest) => {
        dispatch(manualRecreate.request.execute(request));
      },
    };
  },
)(Hook);
