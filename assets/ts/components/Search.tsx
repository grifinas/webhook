import React, { ChangeEvent, Component } from 'react';

interface Props {
  onChange: Function;
}

class Search extends Component<Props> {
  private onChange = ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
    this.props.onChange(value);
  }

  render() {
    return (
      <div className="py-2">
        <input
          className="box w-full p-3 text-sm rounded border outline shadow bg-gray-20"
          type="text"
          placeholder="e.g. SS2-420 ..."
          onChange={this.onChange}
        />
      </div>
    );
  }
}

export default Search;
