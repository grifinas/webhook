import React, { ChangeEvent, Component } from 'react';

interface Props {
  onSubmit: (key: string, value: string) => void;
}

interface State {
  key: string;
  value: string
}

export default class NewEnvRow extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      key: '',
      value: '',
    };
  }

  onKeyChange = (event: ChangeEvent<HTMLInputElement>) => {
    this.setState({
      key: event.target.value,
    });
  }

  onValueChange = (event: ChangeEvent<HTMLInputElement>) => {
    this.setState({
      value: event.target.value,
    });
  }

  onSubmit = () => {
    const { key, value } = this.state;
    if (!key) {
      return;
    }
    this.props.onSubmit(key, value);
    this.setState({
      key: '',
      value: '',
    });
  }

  render() {
    const { key, value } = this.state;

    return (
      <div className="box w-full p-2" key={'new_key_row'}>
        <div className="w-6_12 inline-block">
          <input onChange={this.onKeyChange} value={key} />
        </div>
        <div className="w-6_12 inline-block">
          <input onBlur={this.onSubmit} onChange={this.onValueChange} value={value} />
        </div>
      </div>
    );
  }
}
