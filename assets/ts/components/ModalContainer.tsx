import React, { Component, ReactElement } from 'react';

interface State {
  modal?: ReactElement;
}

export default class ModalContainer extends Component<any, State> {
  constructor(props: never) {
    super(props);

    this.state = {
      modal: undefined,
    };

    window.addEventListener('modalOpen', (event: Event) => {
      this.setState({ modal: (event as CustomEvent<ReactElement>).detail });
    });
  }

  private close = () => {
    this.setState({ modal: undefined });
  }

  render() {
    return this.state.modal ? React.cloneElement(this.state.modal, { onClose: this.close }) : <span/>;
  }
}

export function modal(element: JSX.Element) {
  window.dispatchEvent(new CustomEvent('modalOpen', { detail: element }));
}

