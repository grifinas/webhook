import React, { Component } from 'react';
import ReactSelect, { Props as SelectProps } from 'react-select';

export interface SelectOption {
  value: string;
  label: string;
}

interface Props extends Omit<SelectProps, 'onChange'> {
  onChange: (value: SelectOption) => void;
}

class Select extends Component<Props> {
  render() {
    const { onChange, ...otherProps } = this.props;
    return <ReactSelect { ...otherProps } onChange={ this.onChange } />;
  }

  private onChange = (value: any) => {
    const { onChange } = this.props;
    onChange(value as SelectOption);
  }
}

export default Select;
