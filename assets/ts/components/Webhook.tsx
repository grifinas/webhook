import React, { PureComponent, ReactElement } from 'react';
import { HookInterface } from '../model/webhook';
import Hook from './Hook';

interface Props {
  hooks: HookInterface[];
}

class Webhook extends PureComponent<Props> {
  render() {
    return <div className="w-full my-2 py-2">{this.renderHooks(this.props.hooks)}</div>;
  }

  private renderHooks = (hooks: HookInterface[]): ReactElement[] => {
    const hookElements: ReactElement[] = [];

    for (const idx in hooks) {
      hookElements.push(<Hook key={idx} hook={ hooks[idx] } />);
    }

    return hookElements;
  }
}

export default Webhook;
