import React, { Component } from 'react';
import { faFileAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { modal } from './ModalContainer';
import ModalWebhookLogs from './modals/ModalWebhookLogs';
import ManualCreate from './ManualCreate';

class ActionMenu extends Component {

  render() {
    return (
      <div className="box w-full shadow rounded bg-gray-15 p-2">

        <ManualCreate />

        <div className="inline-block w-1_12 text-right">
          <button className="button btn-base bg-blue" onClick={() => modal(<ModalWebhookLogs branch="webhook"/>)}>
            <FontAwesomeIcon icon={faFileAlt}/>
          </button>
        </div>
      </div>
    );
  }
}

export default ActionMenu;
