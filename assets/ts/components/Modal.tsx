import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

interface Props {
  onClose: Function;
  title?: string;
  contentClass?: string;
}

export default class Modal extends Component<Props> {
  constructor(props: Props) {
    super(props);

    this.state = { display: false };
  }

  render() {
    const { title, children, onClose, contentClass } = this.props;

    return (
      <div>
        <div className="modal-overlay" onClick={() => onClose()}/>
        <div className="modal-container">
          <div className="modal">
            <div className="modal-header">
              {title || ''}
              <FontAwesomeIcon
                icon={faTimes}
                onClick={() => onClose()}
                className="right pointer p-1"
              />
            </div>
            <div className={`modal-content ${contentClass !== undefined ? contentClass : ''}`}>
              {children || ''}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
