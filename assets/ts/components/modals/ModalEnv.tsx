import React, { ChangeEvent, Component } from 'react';
import Modal from '../Modal';
import { connect } from 'react-redux';
import { AppState } from '../../reducer';
import {
  EnvDictionary,
  EnvResponse,
  loadProjectEnvironment,
  saveProjectEnvironment,
} from '../../reducer/environment/actions';
import NewEnvRow from '../NewEnvRow';

interface ConnectedStateProps {
  env?: EnvResponse;
  isSaving: boolean;
}

interface DispatchProps {
  loadProjectEnvironment: () => void;
  saveProjectEnvironment: (env: ModifiedEnv) => void;
}

interface OwnProps {
  id: number;
}

interface Props extends DispatchProps, ConnectedStateProps, OwnProps {
  onClose?: () => {};
}

interface ModifiedEnv extends EnvDictionary {
}

interface State {
  env: ModifiedEnv;
}

enum EnvRowOptions {
  none = 0,
  reversible = 'Undo',
  deletable = 'Delete'
}

class ModalEnv extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      env: {},
    };
  }

  componentDidMount(): void {
    this.props.loadProjectEnvironment();
  }

  onNewEnv = (key: string, value: string) => {
    this.setState({
      env: {
        ...this.state.env,
        [key]: value,
      },
    });
  }

  onValueChange(key: string, event: ChangeEvent<HTMLInputElement>) {
    const value = event.target.value;

    this.setState({
      env: {
        ...this.state.env,
        [key]: value,
      },
    });
  }

  diff(arg1: ModifiedEnv, arg2: EnvResponse): ModifiedEnv {
    const result: ModifiedEnv = {};
    for (const i in arg1) {
      if (!arg2.hasOwnProperty(i)) {
        result[i] = arg1[i] as string;
      }
    }

    return result;
  }

  onSave = () => {
    this.props.saveProjectEnvironment(this.state.env);
  }

  onDelete(key: string) {
    const newEnv = { ...this.state.env };
    delete newEnv[key];

    this.setState({
      env: newEnv,
    });
  }

  render() {
    const { onClose, env, isSaving } = this.props;

    const environmentVariables = [];
    if (env) {
      for (const i in env) {
        const isReversible = this.state.env.hasOwnProperty(i);
        const value = isReversible ? this.state.env[i] : env[i];
        const input = <input value={value} onChange={(event) => this.onValueChange(i, event)} />;

        environmentVariables.push(this.renderEnvRow(i, input, isReversible ? EnvRowOptions.reversible : 0));
      }

      const newEnvs = this.diff(this.state.env, env);

      for (const i in newEnvs) {
        const input = <input value={newEnvs[i]} onChange={(event) => this.onValueChange(i, event)} />;

        environmentVariables.push(this.renderEnvRow(i, input, EnvRowOptions.deletable));
      }
    }

    // the empty row
    environmentVariables.push(<NewEnvRow onSubmit={this.onNewEnv} />);

    return (
      <Modal
        onClose={() => onClose && onClose()}
        title={`Modify environment`}
      >
        {environmentVariables}
        <button className={`button btn-base mt-2 ml-2 ${isSaving ? '' : 'bg-blue'}`} disabled={isSaving} onClick={this.onSave}>Save</button>
      </Modal>
    );
  }

  renderEnvRow(key: string, value: JSX.Element, options: EnvRowOptions = EnvRowOptions.none) {
    return (
      <div className="box w-full p-2" key={key}>
        <div className="w-6_12 inline-block">
          {key}
        </div>
        <div className="w-6_12 inline-block">
          {value}
          {key && options
            ? <button className="button btn-base bg-red" onClick={() => this.onDelete(key)}>{options}</button>
            : null}
        </div>
      </div>
    );
  }
}


export default connect<ConnectedStateProps, DispatchProps, OwnProps, AppState>(
  (state, props) => {
    const { env, isSaving } = state.environment;
    const { id } = props;

    return { isSaving, env: env[id] };
  },
  (dispatch, ownProps) => {
    return {
      loadProjectEnvironment: () => {
        dispatch(loadProjectEnvironment.request.execute({ id: ownProps.id }));
      },
      saveProjectEnvironment: (env: ModifiedEnv) => {
        dispatch(saveProjectEnvironment.request.execute({ id: ownProps.id, env }))
      },
    };
  },
)(ModalEnv);

