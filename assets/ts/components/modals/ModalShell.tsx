import React, { Component } from 'react';
import Modal from '../Modal';

interface Props {
  branch: string;
  onClose?: Function;
}

export default class ModalShell extends Component<Props> {
  render() {
    const { branch, onClose } = this.props;

    return (
      <Modal
        onClose={() => onClose && onClose()}
        title={`Shell: ${branch}`}
      >
        <div className="bg-black text-gray-10 h-full">
          >_
        </div>
      </Modal>
    );
  }
}
