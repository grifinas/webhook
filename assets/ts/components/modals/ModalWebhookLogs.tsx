import React, { Component, ReactElement } from 'react';
import Modal from '../Modal';
import { connect } from 'react-redux';
import { WebhookLog } from '../../model/logs';
import { AppState } from '../../reducer';
import { loadWebhookLogs } from '../../reducer/webhook-log/actions';

interface ConnectedStateProps {
  logs: WebhookLog[];
}

interface DispatchProps {
  loadWebhookLogs: () => void;
}

interface OwnProps {
  branch: string;
  onClose?: Function;
}

interface Props extends DispatchProps, ConnectedStateProps, OwnProps {
}

class ModalWebhookLogs extends Component<Props> {
  componentDidMount(): void {
    this.props.loadWebhookLogs();
  }

  render() {
    const { branch, onClose } = this.props;

    return (
      <Modal
        onClose={() => onClose && onClose()}
        title={`Logs: ${branch}`}
        contentClass="scroll-y"
      >
        {this.renderLogs()}
      </Modal>
    );
  }

  private renderLogs(): ReactElement {
    const logs: ReactElement[] = [];

    this.props.logs.forEach((log: WebhookLog, idx: number) => {
      logs.push(<div key={idx} className="block text-lg">
        <div className="log date inline-block"> [{log.datetime}]</div>
        {log.context.sanitized}
      </div>);
    });

    return <div className="logs">{logs}</div>;
  }
}

export default connect<ConnectedStateProps, DispatchProps, OwnProps, AppState>(
  (state) => {
    return { logs: state.webhookLogs.logs };
  },
  (dispatch, ownProps) => {
    return { loadWebhookLogs: () => dispatch(loadWebhookLogs.request.execute()) };
  })(ModalWebhookLogs);


