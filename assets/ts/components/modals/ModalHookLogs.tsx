import React, { Component, ReactElement } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import Modal from '../Modal';
import { PodLogs, PodLogsRequest } from '../../model/logs';
import { connect } from 'react-redux';
import { AppState } from '../../reducer';
import { loadPodLogs } from '../../reducer/pod-logs/actions';
import { HookInterface } from '../../model/webhook';
import { translateProjectName } from '../../helper/project';

interface OwnProps {
  hook: HookInterface;
  onClose?: Function;
}

interface ConnectedStateProps {
  logs: PodLogs;
}

interface DispatchProps {
  loadPodLogs: (request: PodLogsRequest) => void;
}

interface Props extends DispatchProps, ConnectedStateProps, OwnProps {}

class ModalHookLogs extends Component<Props> {
  componentDidMount(): void {
    const { project, branch } = this.props.hook;
    this.props.loadPodLogs({
      branch,
      project: translateProjectName(project),
      repository: project,
    });
  }

  render() {
    const { branch, project } = this.props.hook;
    const { onClose, logs } = this.props;

    return (
      <Modal
        onClose={() => onClose && onClose()}
        title={`Logs: ${project}\\${branch}`}
      >
        {this.renderTabs()}
      </Modal>
    );
  }

  private renderTabs = (): ReactElement => {
    const { logs } = this.props;

    const tabs: ReactElement[] = [];
    const panel: ReactElement[] = [];

    for (const name in logs) {
      tabs.push(<Tab key={name}> {name} </Tab>);
      const content: string[] = logs[name];
      const formattedLogs: ReactElement[] = content.map((value: string, index: number) => {
        return <div key={`${name}-${index}`}>{value}<br/></div>;
      });

      panel.push(<TabPanel key={`tab-${name}`} className="scroll-y log-tab-content">{formattedLogs}</TabPanel>);
    }

    return <Tabs>
      <TabList>
        {tabs}
      </TabList>
      {panel}
    </Tabs>;
  }
}

export default connect<ConnectedStateProps, DispatchProps, OwnProps, AppState>(
  (state) => {
    return {
      logs: state.podLogs.logs,
    };
  },
  (dispatch, ownProps) => {
    return {
      loadPodLogs: (request: PodLogsRequest) => dispatch(loadPodLogs.request.execute({ request })),
    };
  },
)(ModalHookLogs);
