import React, { PureComponent } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { connect } from 'react-redux';
import { AppState } from '../reducer';
import { manualCreate, ManualCreateRequest } from '../reducer/manual-create/actions';
import { loadProjects } from '../reducer/projects/actions';
import { loadProjectBranches, ProjectBranchesRequest } from '../reducer/project-branches/actions';
import Select, { SelectOption } from './Select';

interface ConnectedStateProps {
  projects: string[];
  projectBranches: string[];
  isCreating: boolean;
}

interface DispatchProps {
  loadProjects: () => void;
  loadProjectBranches: (request: ProjectBranchesRequest) => void;
  manualCreate: (request: ManualCreateRequest) => void;
}

interface OwnProps { }

interface Props extends DispatchProps, ConnectedStateProps, OwnProps { }

interface State {
  selectedProject: SelectOption|null;
  selectedBranch: SelectOption|null;
}

class ManualCreate extends PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      selectedProject: null,
      selectedBranch: null,
    };
  }

  componentDidMount(): void {
    this.props.loadProjects();
  }

  private generateSelectOptions = (options: string[]): SelectOption[] => {
    const selectOptions: SelectOption[] = [];

    if (options) {
      options.forEach((project: string) => {
        selectOptions.push({
          value: project,
          label: project,
        });
      });
    }

    return selectOptions;
  }

  private isProjectSelectDisabled = (): boolean => {
    const { projects, isCreating } = this.props;
    return (!projects.length || isCreating) || false;
  }

  private isProjectBranchesDisabled = (): boolean => {
    const { selectedProject } = this.state;
    const { projectBranches, isCreating } = this.props;
    return !selectedProject || !selectedProject.value || !projectBranches.length || isCreating;
  }

  private isCreateAllowed = (): boolean => {
    const { selectedProject, selectedBranch } = this.state;
    const { isCreating } = this.props;
    return (selectedProject && selectedProject.value && selectedBranch && selectedBranch.value && !isCreating) || false;
  }

  private onProjectChange = (selectedProject: SelectOption) => {
    this.setState({
      selectedProject,
      selectedBranch: null,
    });
    this.props.loadProjectBranches({ projectName: selectedProject.value });
  }

  private onBranchChange = (selectedBranch: SelectOption) => {
    this.setState({ selectedBranch });
  }

  private create = () => {
    const { selectedProject, selectedBranch } = this.state;
    const { isCreating } = this.props;

    if (selectedProject && selectedBranch && selectedProject.value && selectedBranch.value && !isCreating) {
      this.props.manualCreate({
        project: selectedProject.value,
        branch: selectedBranch.value,
      });
    }
  }

  render() {
    return (
      <span>
        <div className="inline-block w-10_12 ">
          <div className="w-4_12 inline-block">
            <Select
              placeholder="Select Project"
              className="pr-1"
              options={this.generateSelectOptions(this.props.projects)}
              onChange={this.onProjectChange}
              isDisabled={this.isProjectSelectDisabled()}
            />
          </div>
          <div className="w-8_12 inline-block">
            <Select
              placeholder="Select Branch"
              options={this.generateSelectOptions(this.props.projectBranches)}
              onChange={this.onBranchChange}
              className="px-1"
              isDisabled={this.isProjectBranchesDisabled()}
              value={this.state.selectedBranch}
            />
          </div>
        </div>
        <div className="inline-block w-1_12">
          <button
            disabled={!this.isCreateAllowed()}
            className={`button btn-base ${ this.isCreateAllowed() ? 'bg-green' : 'bg-gray' } pt-1`}
            onClick={this.create}>
            <FontAwesomeIcon icon={faPlus} className="text-white"/>
          </button>
        </div>
      </span>
    );
  }
}

export default connect<ConnectedStateProps, DispatchProps, OwnProps, AppState>(
  (state) => {
    return {
      projects: state.projects.projects,
      projectBranches: state.projectBranches.branchNames,
      isCreating: state.manualCreate.isCreating,
    };
  },
  (dispatch, ownProps) => {
    return {
      manualCreate: (request: ManualCreateRequest) => {
        dispatch(manualCreate.request.execute(request));
      },
      loadProjects: () => dispatch(loadProjects.request.execute()),
      loadProjectBranches: (request: ProjectBranchesRequest) => {
        dispatch(loadProjectBranches.request.execute(request));
      },
    };
  },
)(ManualCreate);
