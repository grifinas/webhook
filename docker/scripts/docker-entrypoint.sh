#!/bin/bash

# rebuild HAproxy config
build-haproxy-cfg.sh

# setup symfony
composer install && \
    mkdir -p /var/www/html/var/cache && chmod -R 777 /var/www/html/var/cache && \
    mkdir -p /var/www/html/var/log && chmod -R 777 /var/www/html/var/log

# start service
service apache2 start
service haproxy start

# execute whatever was specified in docker's CMD
exec "$@"