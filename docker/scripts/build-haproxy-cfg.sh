#!/bin/bash

BACKEND_FILE="/tmp/backend.cfg"
FRONTEND_FILE="/tmp/fronend.cfg"
CFG_FILE="/etc/haproxy/haproxy.cfg"
CFG_BAREBONE="/etc/haproxy/haproxy.cfg.barebone"

# generate frontend & backend config sections by whatever means necessary
# i.e. by calling external program
# this is only POC, so we will insert dummy content
echo "# POC backends" > $BACKEND_FILE
echo "# POC frontends" > $FRONTEND_FILE

# prepare config
cp $CFG_BAREBONE $CFG_FILE

# inplace edit
sed -i "/# <<<FRONTENDS>>>/ {
        r $FRONTEND_FILE
        g
      }" $CFG_FILE
sed -i "/# <<<BACKENDS>>>/ {
        r $BACKEND_FILE
        g
      }" $CFG_FILE

# boast about our exceptional achievements
echo "haproxy.cfg updated"
