<?php
declare(strict_types=1);

namespace App\Model;

class LockedFileModel
{
    private $file;

    public function __construct(string $file, string $mode = 'c+')
    {
        $this->file = fopen($file, $mode);
        flock($this->file, LOCK_EX);
    }

    public function __destruct()
    {
        flock($this->file, LOCK_UN);
        fclose($this->file);
    }

    public function truncate(int $size = 0)
    {
        ftruncate($this->file, $size);
    }

    public function seek(int $offset)
    {
        fseek($this->file, $offset);
    }

    public function write(string $content)
    {
        fwrite($this->file, $content);
    }
}
