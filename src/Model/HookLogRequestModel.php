<?php
declare(strict_types=1);

namespace App\Model;

use JMS\Serializer\Annotation as Serializer;

class HookLogRequestModel
{
    /** @Serializer\Type("string") */
    public string $project;
    /** @Serializer\Type("string") */
    public string $branch;
    /** @Serializer\Type("string") */
    public string $repository;
}
