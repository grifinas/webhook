<?php
declare(strict_types=1);

namespace App\Model;

use JMS\Serializer\Annotation as Serializer;

class PushEventModel
{
    /**
     * @Serializer\Type("App\Model\ReferenceStateModel")
     */
    public ReferenceStateModel $old;

    /**
     * @Serializer\Type("App\Model\ReferenceStateModel")
     */
    public ReferenceStateModel $new;

    /**
     * @Serializer\Type("array")
     * @var array
     */
    public $links;

    /**
     * @Serializer\Type("boolean")
     */
    public bool $created;

    /**
     * @Serializer\Type("boolean")
     */
    public bool $closed;

    /**
     * @Serializer\Type("boolean")
     */
    public bool $forced;

    /**
     * @Serializer\Type("array")
     * @var array
     */
    public $commits = [];

    /**
     * @Serializer\Type("boolean")
     */
    public bool $truncated;
}
