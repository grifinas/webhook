<?php
declare(strict_types=1);

namespace App\Model;

use DateTime;
use JMS\Serializer\Annotation as Serializer;

class HookModel
{
    /**
     * @Serializer\Type("int")
     */
    private ?int $id = null;
    /**
     * @Serializer\Type("string")
     */
    private string $branch;
    /**
     * @Serializer\Type("string")
     */
    private string $project;
    /**
     * @Serializer\Type("string")
     */
    private string $name;
    /**
     * @Serializer\Type("string")
     */
    private string $link;
    /**
     * @Serializer\Type("string")
     */
    private string $root = '';
    /**
     * @Serializer\Type("DateTime")
     */
    private DateTime $createdAt;

    public function __construct(string $project, string $branch, DateTime $createdAt = null)
    {
        $this->branch = $branch;
        $this->project = $project;
        $this->createdAt = $createdAt ?? new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getBranch(): string
    {
        return $this->branch;
    }

    public function setBranch(string $branch): self
    {
        $this->branch = $branch;
        return $this;
    }

    public function getProject(): string
    {
        return $this->project;
    }

    public function setProject(string $project): self
    {
        $this->project = $project;
        return $this;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;
        return $this;
    }

    public function getRoot(): string
    {
        return $this->root;
    }

    public function setRoot(string $root): HookModel
    {
        $this->root = $root;
        return $this;
    }
}
