<?php
declare(strict_types=1);

namespace App\Model;

use JMS\Serializer\Annotation as Serializer;

class PutEnvModel
{
    /**
     * @Serializer\Type("array")
     */
    public $environment;
}
