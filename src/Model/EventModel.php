<?php
declare(strict_types=1);

namespace App\Model;

use JMS\Serializer\Annotation as Serializer;

class EventModel
{
    /**
     * @Serializer\Type("string")
     */
    public string $actor;

    /**
     * @Serializer\Type("App\Model\Repository")
     */
    public Repository $repository;

    /**
     * @Serializer\Type("App\Model\PushEventCollectionModel")
     */
    public PushEventCollectionModel $push;
}
