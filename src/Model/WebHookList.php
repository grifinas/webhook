<?php
declare(strict_types=1);

namespace App\Model;

use App\Service\Collection;
use JMS\Serializer\Annotation as Serializer;

class WebHookList
{
    /**
     * @Serializer\Accessor(getter="toArray", setter="setHooks")
     * @Serializer\Type("array<App\Model\HookModel>")
     * @var HookModel[]|Collection
     */
    private $hooks;

    public function __construct()
    {
        $this->hooks = new Collection();
    }

    public function getHooks(): Collection
    {
        return $this->hooks;
    }

    public function setHooks(array $hooks): self
    {
        $this->hooks = new Collection($hooks);
        return $this;
    }

    /**
     * @param HookModel $hook
     * @return $this
     */
    public function addHook(HookModel $hook): self
    {
        if ($this->has($hook) === false) {
            $this->hooks->add($hook);
        }

        return $this;
    }

    public function removeHook(HookModel $hook): self
    {
        $key = $this->has($hook);
        if ($key !== false) {
            $this->hooks->remove($key);
        }

        return $this;
    }

    public function toArray()
    {
        return $this->hooks->list();
    }

    /**
     * @param HookModel $hookModel
     * @return false|int
     */
    public function has(HookModel $hookModel)
    {
        foreach ($this->hooks->list() as $key => $hook) {
            if ($hookModel->getBranch() === $hook->getBranch()
                && $hookModel->getLink() === $hook->getLink()
                && $hookModel->getName() === $hook->getName()
                && $hookModel->getProject() === $hook->getProject()
            ) {
                return $key;
            }
        }

        return false;
    }
}
