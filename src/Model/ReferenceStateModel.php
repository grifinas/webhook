<?php
declare(strict_types=1);

namespace App\Model;

use JMS\Serializer\Annotation as Serializer;

class ReferenceStateModel
{
    public const TYPE_BRANCH = 'branch';
    public const TYPE_TAG = 'tag';

    /**
     * @Serializer\Type("string")
     * @var string
     */
    public $type;

    /**
     * @Serializer\Type("string")
     * @var string
     */
    public $name;

    /**
     * @Serializer\Type("array")
     * @var array
     */
    public $target;

    /**
     * @Serializer\Type("array")
     * @var array
     */
    public $links;
}
