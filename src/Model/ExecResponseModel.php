<?php
declare(strict_types=1);

namespace App\Model;

class ExecResponseModel
{
    public const RESULT_OK = 0;

    public int $result;
    /**
     * @var string[]
     */
    public $output;

    /**
     * ExecResponseModel constructor.
     * @param int $result
     * @param string[] $output
     */
    public function __construct(int $result, array $output)
    {
        $this->result = $result;
        $this->output = $output;
    }
}
