<?php
declare(strict_types=1);

namespace App\Model;

use JMS\Serializer\Annotation as Serializer;

class Repository
{
    /**
     * @Serializer\Type("string")
     */
    public string $type;
    /**
     * @Serializer\Type("string")
     */
    public string $name;
    /**
     * @Serializer\Type("string")
     */
    public string $fullName;
    /**
     * @Serializer\Type("string")
     */
    public string $uuid;
    /**
     * @Serializer\Type("array")
     */
    public array $links;
    /**
     * @Serializer\Type("array")
     */
    public array $project;
    /**
     * @Serializer\Type("string")
     */
    public string $website;
    /**
     * @Serializer\Type("array")
     */
    public array $owner;
    /**
     * @Serializer\Type("string")
     */
    public string $scm;
    /**
     * @Serializer\Type("string")
     */
    public string $isPrivate;
}
