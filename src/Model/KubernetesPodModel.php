<?php
declare(strict_types=1);

namespace App\Model;

class KubernetesPodModel
{
    public const STATUS_RUNNING = 'Running';

    public string $name;
    public string $status;
    public string $namespace;

    public function __construct(string $name, string $status, string $namespace)
    {
        $this->name = $name;
        $this->status = $status;
        $this->namespace = $namespace;
    }
}
