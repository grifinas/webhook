<?php
declare(strict_types=1);

namespace App\Model;

class ProjectModel
{
    private string $project;
    private string $root;
    private string $branch;

    public function __construct(string $project, string $branch, string $root)
    {
        $this->project = $project;
        $this->branch = mb_strtolower($branch);
        $this->root = $root;
    }

    public function getProject(): string
    {
        return $this->project;
    }

    public function getRoot(): string
    {
        return $this->root;
    }

    public function getBranch(): string
    {
        return $this->branch;
    }
}
