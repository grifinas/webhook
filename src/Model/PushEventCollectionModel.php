<?php
declare(strict_types=1);

namespace App\Model;

use JMS\Serializer\Annotation as Serializer;

class PushEventCollectionModel
{
    /**
     * @Serializer\Type("array<App\Model\PushEventModel>")
     * @var PushEventModel[]
     */
    public $changes;
}
