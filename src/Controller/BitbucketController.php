<?php
declare(strict_types=1);

namespace App\Controller;

use App\Exception\ValidationException;
use App\Model\EventModel;
use App\Service\PushHandler;
use App\Service\RequestDeserializer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BitbucketController extends AbstractController
{
    private RequestDeserializer $deserializer;
    private PushHandler $handler;

    public function __construct(RequestDeserializer $deserializer, PushHandler $handler)
    {
        $this->deserializer = $deserializer;
        $this->handler = $handler;
    }

    /**
     * @Route("/push", name="push")
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function push(Request $request): Response
    {
        /** @var EventModel $model */
        $model = $this->deserializer->deserialize($request, EventModel::class);

        $this->handler->handle($model);

        return new Response(null, Response::HTTP_NO_CONTENT);
    }
}
