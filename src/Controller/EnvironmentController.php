<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Webhook;
use App\Exception\StrategyNotFoundException;
use App\Exception\ValidationException;
use App\Model\PutEnvModel;
use App\Service\RequestDeserializer;
use App\Strategy\EnvironmentProvider;
use App\Transformer\WebhookTransformer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EnvironmentController extends AbstractController
{
    private RequestDeserializer $deserializer;
    private WebhookTransformer $transformer;
    private EnvironmentProvider $environmentProvider;

    public function __construct(
        RequestDeserializer $deserializer,
        EnvironmentProvider $environmentProvider,
        WebhookTransformer $transformer
    ) {
        $this->deserializer = $deserializer;
        $this->environmentProvider = $environmentProvider;
        $this->transformer = $transformer;
    }

    /**
     * @Route(path="/webhook/{webhook}/env", name="get-env", methods={"GET"})
     * @param Webhook $webhook
     * @return Response
     * @throws StrategyNotFoundException
     */
    public function getEnv(Webhook $webhook): Response
    {
        $model = $this->transformer->toModel($webhook);
        $environmentStrategy = $this->environmentProvider->get($model);
        return $this->json($environmentStrategy->getEnv($model));
    }

    /**
     * @Route(path="/webhook/{webhook}/env", name="put-env", methods={"POST"})
     * @param Webhook $webhook
     * @param Request $request
     * @return Response
     * @throws ValidationException
     * @throws StrategyNotFoundException
     */
    public function putEnv(Webhook $webhook, Request $request): Response
    {
        $model = $this->transformer->toModel($webhook);
        /** @var PutEnvModel $model */
        $env = $this->deserializer->deserialize($request, PutEnvModel::class);

        $environmentStrategy = $this->environmentProvider->get($model);

        $environmentStrategy->putEnv($model, $env);

        return new Response(null, Response::HTTP_NO_CONTENT);
    }
}
