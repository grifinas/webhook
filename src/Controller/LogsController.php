<?php
declare(strict_types=1);

namespace App\Controller;

use App\Exception\ValidationException;
use App\Model\HookLogRequestModel;
use App\Service\LogsProvider;
use App\Service\RequestDeserializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class LogsController extends AbstractController
{
    private LogsProvider $logsProvider;
    private RequestDeserializer $deserializer;

    public function __construct(LogsProvider $logsProvider, RequestDeserializer $deserializer)
    {
        $this->logsProvider = $logsProvider;
        $this->deserializer = $deserializer;
    }

    /**
     * @Route("/webhook-log", name="webhook_log")
     * @return Response
     */
    public function webhookLogs(): Response
    {
        return new Response($this->logsProvider->getWebhookLog());
    }

    /**
     * @Route("/hook-log", name="hook_log")
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function getHookLogs(Request $request): Response
    {
        /** @var $model HookLogRequestModel */
        $model = $this->deserializer->deserialize($request, HookLogRequestModel::class);

        return $this->json($this->logsProvider->getHookLogs($model));
    }
}
