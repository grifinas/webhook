<?php
declare(strict_types=1);

namespace App\Controller;

use App\Exception\WebhookHandlerException;
use App\Service\WebhookCache;
use App\Service\WebhookService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WebhookController extends AbstractController
{
    private WebhookService $webhookService;
    private WebhookCache $webhookCache;

    public function __construct(WebhookService $webhookService, WebhookCache $webhookCache)
    {
        $this->webhookService = $webhookService;
        $this->webhookCache = $webhookCache;
    }

    /**
     * @Route("/webhooks", name="webhooks")
     */
    public function getWebhooks(): Response
    {
        return $this->json($this->webhookCache->get()->toArray());
    }

    /**
     * @Route("/manual-create/{repository}/{branch}", name="manual_create")
     * @param string $repository
     * @param string $branch
     * @return Response
     * @throws WebhookHandlerException
     */
    public function create(string $repository, string $branch): Response
    {
        $this->webhookService->create($repository, $branch);
        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/manual-delete/{repository}/{branch}", name="manual_delete")
     * @param string $repository
     * @param string $branch
     * @return Response
     * @throws WebhookHandlerException
     */
    public function delete(string $repository, string $branch): Response
    {
        $this->webhookService->delete($repository, $branch);
        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/manual-recreate/{repository}/{branch}", name="manual_recreate")
     * @param string $repository
     * @param string $branch
     * @return Response
     */
    public function recreate(string $repository, string $branch): Response
    {
        $this->webhookService->recreate($repository, $branch);
        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/get-branches/{repository}", name="get_branches")
     * @param string $repository
     * @return Response
     */
    public function getBranches(string $repository): Response
    {
        return $this->json($this->webhookService->getBranches($repository));
    }

    /**
     * @Route("/get-projects", name="get_projects")
     * @return Response
     */
    public function getProjects(): Response
    {
        return $this->json($this->webhookService->getProjects());
    }
}
