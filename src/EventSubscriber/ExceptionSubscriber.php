<?php
declare(strict_types=1);

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionSubscriber implements EventSubscriberInterface
{
    public function handle(ExceptionEvent $event)
    {
//        $event->setResponse(new JsonResponse([
//           'error' => [
//               'message' => $event->getThrowable()->getMessage(),
//           ],
//        ]));
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => ['handle', 10],
        ];
    }
}
