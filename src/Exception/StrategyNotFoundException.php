<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;

class StrategyNotFoundException extends Exception
{
    protected $message = 'No strategy was found for repository: "%s"';

    public function __construct($repository = '')
    {
        parent::__construct(sprintf($this->message, $repository), 1);
    }
}
