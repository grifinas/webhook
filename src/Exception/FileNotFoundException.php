<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;

class FileNotFoundException extends Exception
{
    protected $message = 'File "%s" was not found';

    public function __construct($file)
    {
        parent::__construct(sprintf($this->message, $file), 1);
    }
}
