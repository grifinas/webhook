<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationException extends Exception
{
    private ConstraintViolationListInterface $violationList;
    protected $message = 'There were %i violations when validating';

    public function __construct(ConstraintViolationListInterface $violationList)
    {
        $this->violationList = $violationList;

        parent::__construct(sprintf($this->message, $violationList->count()), 1);
    }

    /**
     * @return ConstraintViolationListInterface
     */
    public function getViolationList(): ConstraintViolationListInterface
    {
        return $this->violationList;
    }
}
