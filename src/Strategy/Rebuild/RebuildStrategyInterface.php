<?php
declare(strict_types=1);

namespace App\Strategy\Rebuild;

use App\Model\HookModel;

interface RebuildStrategyInterface
{
    public function recreate(HookModel $model);
}
