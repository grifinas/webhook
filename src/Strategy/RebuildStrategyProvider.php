<?php
declare(strict_types=1);

namespace App\Strategy;

use App\Exception\StrategyNotFoundException;
use App\Model\HookModel;
use App\Service\FileSystem;
use App\Strategy\Rebuild\RebuildStrategyInterface;

class RebuildStrategyProvider
{
    private FileSystem $fileSystem;

    public function __construct(
        FileSystem $fileSystem
    ) {
        $this->fileSystem = $fileSystem;
    }

    public function get(HookModel $model): RebuildStrategyInterface
    {
        // Todo: logic for rebuilding
        throw new StrategyNotFoundException($model->getProject());
    }
}
