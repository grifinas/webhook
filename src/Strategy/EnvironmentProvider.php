<?php
declare(strict_types=1);

namespace App\Strategy;

use App\Exception\StrategyNotFoundException;
use App\Model\HookModel;
use App\Service\FileSystem;
use App\Strategy\Environment\EnvironmentStrategyInterface;
use App\Strategy\Environment\K8sApacheEnvStrategy;
use App\Strategy\Environment\SS2SpaEnvStrategy;

class EnvironmentProvider
{
    private const REPOSITORY_SS2_SPA = 'shipment-system-spa';

    private string $root;
    private K8sApacheEnvStrategy $apacheEnvStrategy;
    private $strategies;
    private FileSystem $fileSystem;

    public function __construct(
        string $branchDir,
        K8sApacheEnvStrategy $apacheEnvStrategy,
        SS2SpaEnvStrategy $ss2SpaEnvStrategy,
        FileSystem $fileSystem
    ) {
        $this->apacheEnvStrategy = $apacheEnvStrategy;
        $this->strategies = [
            self::REPOSITORY_SS2_SPA => $ss2SpaEnvStrategy,
        ];
        $this->fileSystem = $fileSystem;
        $this->root = $branchDir;
    }

    public function get(HookModel $model): EnvironmentStrategyInterface
    {
        // TODO use strategy selector, maybe?
        $strategy = $this->strategies[$model->getProject()] ?? null;

        if ($strategy) {
            return $strategy;
        }

        if ($this->fileSystem->exists("{$model->getRoot()}/k8s")) {
            return $this->apacheEnvStrategy;
        }

        throw new StrategyNotFoundException($model->getProject());
    }
}
