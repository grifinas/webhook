<?php
declare(strict_types=1);

namespace App\Strategy\Log;

use App\Model\HookModel;
use App\Service\FileSystem;
use App\Service\Kubernetes;
use Symfony\Component\Yaml\Yaml;

class K8SLogsStrategy implements LogsStrategyInterface
{
    private Kubernetes $kubernetes;
    private FileSystem $fileSystem;

    public function __construct(Kubernetes $kubernetes, FileSystem $fileSystem)
    {
        $this->kubernetes = $kubernetes;
        $this->fileSystem = $fileSystem;
    }

    /**
     * @inheritDoc
     */
    public function getLogs(HookModel $model): array
    {
        $logs = [];

        $pod = $this->kubernetes->findPod($model->getName(), $model->getBranch());

        if ($pod) {
            $path = "{$model->getRoot()}/k8s/dev/deployment.yaml";
            $confs = Yaml::parse(
                $this->fileSystem->get($path)
            );

            foreach ($confs['spec']['template']['spec']['containers'] ?? [] as $conf) {

                if (isset($conf['name'])) {
                    $logs[$conf['name']] = $this->kubernetes->getContainerLog($pod, $conf['name']);
                }
            }
        }
        return $logs;
    }
}
