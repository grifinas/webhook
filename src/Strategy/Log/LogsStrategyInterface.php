<?php
declare(strict_types=1);

namespace App\Strategy\Log;

use App\Model\HookModel;

interface LogsStrategyInterface
{
    /**
     * @param HookModel $model
     * @return string[]
     */
    public function getLogs(HookModel $model): array;
}
