<?php
declare(strict_types=1);

namespace App\Strategy\Environment;

use App\Model\HookModel;
use App\Model\PutEnvModel;

interface EnvironmentStrategyInterface
{
    public function getEnv(HookModel $model): array;

    public function putEnv(HookModel $model, PutEnvModel $envModel): void;
}
