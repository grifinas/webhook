<?php
declare(strict_types=1);

namespace App\Strategy\Environment;

use App\Model\HookModel;
use App\Model\PutEnvModel;
use App\Service\FileSystem;
use App\Service\Kubernetes;
use App\Service\KubernetesFileSystem;
use App\Strategy\Create\SS2SpaCreateStrategy;
use App\Strategy\Delete\K8SDeleteStrategy;

class SS2SpaEnvStrategy extends GenericK8SEnvStrategy
{
    private SS2SpaCreateStrategy $spaCreateStrategy;
    private K8SDeleteStrategy $k8SDeleteStrategy;

    public function __construct(
        Kubernetes $kubernetes,
        KubernetesFileSystem $kubernetesFileSystem,
        FileSystem $fileSystem,
        SS2SpaCreateStrategy $spaCreateStrategy,
        K8SDeleteStrategy $k8SDeleteStrategy
    ) {
        parent::__construct($kubernetes, $kubernetesFileSystem, $fileSystem);

        $this->spaCreateStrategy = $spaCreateStrategy;
        $this->k8SDeleteStrategy = $k8SDeleteStrategy;
    }

    public function putEnv(HookModel $model, PutEnvModel $envModel): void
    {
        $this->writeToLocalEnv($model, $envModel);

        $model->setName($this->kubernetesFileSystem->getProjectName($model));

        $this->k8SDeleteStrategy->deleteConfigs($model);
        $this->spaCreateStrategy->applyConfigs($model);
    }
}
