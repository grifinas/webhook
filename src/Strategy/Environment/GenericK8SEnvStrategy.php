<?php
declare(strict_types=1);

namespace App\Strategy\Environment;

use App\Model\HookModel;
use App\Model\LockedFileModel;
use App\Model\PutEnvModel;
use App\Service\FileSystem;
use App\Service\Kubernetes;
use App\Service\KubernetesFileSystem;
use Exception;
use Symfony\Component\Dotenv\Dotenv;

abstract class GenericK8SEnvStrategy implements EnvironmentStrategyInterface
{
    private FileSystem $fileSystem;
    protected Kubernetes $kubernetes;
    protected KubernetesFileSystem $kubernetesFileSystem;

    public function __construct(
        Kubernetes $kubernetes,
        KubernetesFileSystem $kubernetesFileSystem,
        FileSystem $fileSystem
    ) {
        $this->kubernetes = $kubernetes;
        $this->fileSystem = $fileSystem;
        $this->kubernetesFileSystem = $kubernetesFileSystem;
    }

    public function getEnv(HookModel $model): array
    {
        $pod = $this->kubernetes->getPod($model->getName(), $model->getBranch());
        $podEnv = $this->kubernetes->getEnv($pod);

        return array_merge(
            $this->getEnvFileContents($model->getRoot()),
            $podEnv,
            $this->getEnvFileContents($model->getRoot(), '.env.local')
        );
    }

    abstract public function putEnv(HookModel $model, PutEnvModel $envModel): void;

    protected function getEnvFileContents(string $projectRoot, string $envName = '.env'): array
    {
        $path = "$projectRoot/$envName";
        if (!$this->fileSystem->exists($path)) {
            return [];
        }

        try {
            return (new Dotenv())->parse($this->fileSystem->get($path));
        } catch (Exception $e) {
            return [];
        }
    }

    protected function writeToLocalEnv(HookModel $model, PutEnvModel $envModel): void
    {

        $file = new LockedFileModel("{$model->getRoot()}/.env.local");

        $local = $this->getEnvFileContents($model->getRoot(), '.env.local');

        foreach ($envModel->environment as $key => $value) {
            $local[$key] = $value;
        }

        $file->truncate(0);
        $file->seek(0);
        foreach ($local as $key => $value) {
            $file->write("$key=\"$value\"\n");
        }
    }
}
