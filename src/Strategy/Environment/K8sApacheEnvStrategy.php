<?php
declare(strict_types=1);

namespace App\Strategy\Environment;

use App\Model\HookModel;
use App\Model\PutEnvModel;

class K8sApacheEnvStrategy extends GenericK8SEnvStrategy
{
    public function putEnv(HookModel $model, PutEnvModel $envModel): void
    {
        $this->writeToLocalEnv($model, $envModel);

        $projectRoot = "{$this->root}/{$model->getProject()}/{$model->getBranch()}";
        $name = $this->kubernetesFileSystem->getProjectName($projectRoot);
        $pod = $this->kubernetes->getPod($name, $model->getBranch());
        $this->kubernetes->restartApache($pod);
    }
}
