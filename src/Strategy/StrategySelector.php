<?php
declare(strict_types=1);

namespace App\Strategy;

use App\Exception\StrategyNotFoundException;
use App\Model\HookModel;

class StrategySelector
{
    public function select(HookModel $model, array $strategies)
    {
        if (isset($strategies[$model->getProject()])) {
            return $strategies[$model->getProject()];
        }

        throw new StrategyNotFoundException($model->getProject());
    }
}
