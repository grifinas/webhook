<?php
declare(strict_types=1);

namespace App\Strategy\Update;

use App\Exception\PodStateException;
use App\Model\HookModel;
use App\Model\KubernetesPodModel;
use App\Service\Kubernetes;
use App\Service\KubernetesFileSystem;

class K8SUpdateStrategy implements UpdateStrategyInterface
{
    protected Kubernetes $kubernetes;
    protected KubernetesFileSystem $kubernetesFileSystem;

    public function __construct(
        Kubernetes $kubernetes,
        KubernetesFileSystem $kubernetesFileSystem
    ) {
        $this->kubernetes = $kubernetes;
        $this->kubernetesFileSystem = $kubernetesFileSystem;
    }

    public function update(HookModel $model)
    {
        $name = $this->kubernetesFileSystem->getProjectName($model);

        $pod = $this->kubernetes->findPod($name, $model->getBranch());
        if (!$pod || $pod->status !== KubernetesPodModel::STATUS_RUNNING) {
            throw new PodStateException('Pod is not found or not running');
        }

        $this->kubernetes->runMigrations($pod);
    }
}
