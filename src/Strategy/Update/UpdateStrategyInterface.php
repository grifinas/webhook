<?php
declare(strict_types=1);

namespace App\Strategy\Update;

use App\Model\HookModel;

interface UpdateStrategyInterface
{
    public function update(HookModel $model);
}
