<?php
declare(strict_types=1);

namespace App\Strategy\Delete;

use App\Model\HookModel;

interface DeleteStrategyInterface
{
    public function delete(HookModel $model);
}
