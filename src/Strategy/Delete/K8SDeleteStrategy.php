<?php
declare(strict_types=1);

namespace App\Strategy\Delete;

use App\Exception\RemoveWebhookFileSystemException;
use App\Exception\RemoveWebhookKubernetesException;
use App\Model\HookModel;
use App\Service\FileSystem;
use App\Service\Kubernetes;
use App\Service\KubernetesFileSystem;

class K8SDeleteStrategy implements DeleteStrategyInterface
{
    protected const CONFIG_DIRS = [
        'k8s/dev',
    ];
    protected const YAML_CONFIG_ORDER = [
        'secrets',
        'deployment',
        'services',
        'ingress',
    ];
    private const DEV_NAMESPACE = 'namespace: venipak';

    private FileSystem $fileSystem;

    private Kubernetes $kubernetes;
    private KubernetesFileSystem $kubernetesFileSystem;

    public function __construct(
        FileSystem $fileSystem,
        Kubernetes $kubernetes,
        KubernetesFileSystem $kubernetesFileSystem
    ) {
        $this->kubernetes = $kubernetes;
        $this->kubernetesFileSystem = $kubernetesFileSystem;
        $this->fileSystem = $fileSystem;
    }

    /**
     * @inheritDoc
     */
    public function delete(HookModel $model)
    {
        $this->validate($model);

        $pod = $this->kubernetes->findPod($model->getName(), $model->getBranch());
        $pods = $this->kubernetes->getNamespacePods($pod->namespace);

        if (count($pods) <= 1) {
            $this->kubernetes->deleteNamespace($pod->namespace);
            return;
        }

        $this->deleteConfigs($model);
    }

    public function deleteConfigs(HookModel $model): void
    {
        $configs = $this->kubernetesFileSystem->getConfigs(
            $model->getRoot(),
            $this::YAML_CONFIG_ORDER,
            $this::CONFIG_DIRS
        );

        foreach ($configs as $config) {
            $this->kubernetesFileSystem->makeYaml($model, $config);
            $this->kubernetes->delete($config);
        }
    }

    private function validate(HookModel $hook): void
    {
        if (!$this->fileSystem->isDir($hook->getRoot())) {
            throw new RemoveWebhookFileSystemException('No branch directory to delete.');
        }

        if ($this->kubernetes->findPod($hook->getName(), $hook->getBranch()) === null) {
            throw new RemoveWebhookKubernetesException('Pod to delete not found.');
        }
    }
}
