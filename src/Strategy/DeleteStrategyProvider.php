<?php
declare(strict_types=1);

namespace App\Strategy;

use App\Exception\StrategyNotFoundException;
use App\Model\HookModel;
use App\Service\FileSystem;
use App\Strategy\Delete\DeleteStrategyInterface;
use App\Strategy\Delete\K8SDeleteStrategy;

class DeleteStrategyProvider
{
    private FileSystem $fileSystem;

    private K8SDeleteStrategy $k8SDeleteStrategy;

    public function __construct(
        FileSystem $fileSystem,
        K8SDeleteStrategy $k8SDeleteStrategy
    ) {
        $this->fileSystem = $fileSystem;
        $this->k8SDeleteStrategy = $k8SDeleteStrategy;
    }

    public function get(HookModel $model): DeleteStrategyInterface
    {
        if ($this->fileSystem->isDir("{$model->getRoot()}/k8s")) {
            return $this->k8SDeleteStrategy;
        }

        throw new StrategyNotFoundException($model->getProject());
    }
}
