<?php
declare(strict_types=1);

namespace App\Strategy;

use App\Exception\StrategyNotFoundException;
use App\Model\HookModel;
use App\Service\FileSystem;
use App\Strategy\Update\K8SUpdateStrategy;
use App\Strategy\Update\UpdateStrategyInterface;

class UpdateStrategyProvider
{
    private FileSystem $fileSystem;

    private K8SUpdateStrategy $k8SUpdateStrategy;

    public function __construct(
        FileSystem $fileSystem,
        K8SUpdateStrategy $k8SUpdateStrategy
    ) {
        $this->fileSystem = $fileSystem;
        $this->k8SUpdateStrategy = $k8SUpdateStrategy;
    }

    public function get(HookModel $model): UpdateStrategyInterface
    {
        if ($this->fileSystem->isDir("{$model->getRoot()}/k8s")) {
            return $this->k8SUpdateStrategy;
        }

        throw new StrategyNotFoundException($model->getProject());
    }
}
