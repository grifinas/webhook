<?php
declare(strict_types=1);

namespace App\Strategy;

use App\Exception\StrategyNotFoundException;
use App\Model\HookModel;
use App\Service\FileSystem;
use App\Strategy\Log\K8SLogsStrategy;
use App\Strategy\Log\LogsStrategyInterface;

class LogsStrategyProvider
{
    private FileSystem $fileSystem;
    private K8SLogsStrategy $k8SLogsStrategy;

    public function __construct(
        FileSystem $fileSystem,
        K8SLogsStrategy $k8SLogsStrategy
    ) {
        $this->fileSystem = $fileSystem;
        $this->k8SLogsStrategy = $k8SLogsStrategy;
    }

    public function get(HookModel $model): LogsStrategyInterface
    {
        if ($this->fileSystem->isDir("{$model->getRoot()}/k8s")) {
            return $this->k8SLogsStrategy;
        }

        throw new StrategyNotFoundException($model->getProject());
    }
}
