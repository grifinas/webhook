<?php
declare(strict_types=1);

namespace App\Strategy;

use App\Exception\StrategyNotFoundException;
use App\Model\HookModel;
use App\Service\FileSystem;
use App\Strategy\Create\CreateStrategyInterface;
use App\Strategy\Create\K8SCreateStrategy;
use App\Strategy\Create\SS2ApiCreateStrategy;
use App\Strategy\Create\SS2SpaCreateStrategy;
use App\Strategy\Create\UpsAdapterCreateStrategy;

class CreateStrategyProvider
{
    public const REPOSITORY_SS2_API = 'shipment-system-api';
    public const REPOSITORY_SS2_SPA = 'shipment-system-spa';
    public const REPOSITORY_UPS_ADAPTER = 'ups-adapter';

    private StrategySelector $selector;
    private FileSystem $fileSystem;
    private K8SCreateStrategy $k8SCreateStrategy;
    private array $strategies;

    public function __construct(
        StrategySelector $selector,
        FileSystem $fileSystem,
        K8SCreateStrategy $k8SCreateStrategy,
        SS2ApiCreateStrategy $SS2ApiCreateStrategy,
        SS2SpaCreateStrategy $SS2SpaCreateStrategy,
        UpsAdapterCreateStrategy $upsAdapterCreateStrategy
    ) {
        $this->selector = $selector;
        $this->fileSystem = $fileSystem;
        $this->k8SCreateStrategy = $k8SCreateStrategy;

        $this->strategies = [
            self::REPOSITORY_SS2_API => $SS2ApiCreateStrategy,
            self::REPOSITORY_SS2_SPA => $SS2SpaCreateStrategy,
            self::REPOSITORY_UPS_ADAPTER => $upsAdapterCreateStrategy,
        ];
    }

    public function get(HookModel $model): CreateStrategyInterface
    {
        try {
            return $this->selector->select($model, $this->strategies);
        } catch (StrategyNotFoundException $exception) {
            if ($this->fileSystem->isDir("{$model->getRoot()}/k8s")) {
                return $this->k8SCreateStrategy;
            }

            throw $exception;
        }
    }
}
