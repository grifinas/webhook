<?php
declare(strict_types=1);

namespace App\Strategy\Create;

use App\Model\HookModel;
use App\Service\Docker;
use App\Service\FileSystem;
use App\Service\Kubernetes;
use App\Service\KubernetesFileSystem;

class K8SCreateStrategy implements CreateStrategyInterface
{
    public const CONFIG_DIRS = [
        'k8s/dev',
    ];
    public const YAML_CONFIG_ORDER = [
        'secrets',
        'deployment',
        'services',
        'ingress',
    ];

    protected const BUILD_TARGET = 'dev';

    private const DEV_NAMESPACE = 'namespace: venipak';

    protected Kubernetes $kubernetes;
    protected Docker $docker;
    protected FileSystem $fileSystem;
    protected KubernetesFileSystem $kubernetesFileSystem;

    public function __construct(
        Kubernetes $kubernetes,
        Docker $docker,
        FileSystem $fileSystem,
        KubernetesFileSystem $kubernetesFileSystem
    ) {
        $this->kubernetes = $kubernetes;
        $this->docker = $docker;
        $this->fileSystem = $fileSystem;
        $this->kubernetesFileSystem = $kubernetesFileSystem;
    }

    public function create(HookModel $model): HookModel
    {
        $this->kubernetesFileSystem->copySamplesToYaml($model, self::DEV_NAMESPACE, $this::CONFIG_DIRS);

        $this->kubernetes->createNamespace($model->getBranch());
        $this->kubernetes
            ->generateSecret()
            ->idRsa($model->getBranch())
            ->ssl($model->getBranch())
        ;

        $model->setName($this->kubernetesFileSystem->getProjectName($model));
        $model->setLink($this->kubernetesFileSystem->getLink($model));

        $this->buildImages($model->getRoot(), $model->getName());

        $this->applyConfigs($model);

        return $model;
    }

    public function applyConfigs(HookModel $model): void
    {
        $configs = $this->kubernetesFileSystem->getConfigs(
            $model->getRoot(),
            $this::YAML_CONFIG_ORDER,
            $this::CONFIG_DIRS
        );

        foreach ($configs as $config) {
            $this->kubernetesFileSystem->makeYaml($model, $config);
            $this->kubernetes->apply($config);
        }
    }

    protected function buildImages(string $root, string $name): void
    {
        $dockerfilePath = "$root/docker/Dockerfile";
        $dockerfile = $this->fileSystem->get($dockerfilePath);
        $dockerfile = str_replace(
            'composer install &&',
            'composer install && \
            bin/console doctrine:migrations:migrate --allow-no-migration -n && \
            bin/console doctrine:fixtures:load -n &&',
            $dockerfile
        );

        // Prepare for easy environment editing
        $dockerfile .= "\nRUN echo '. /var/www/html/.env.local >> /etc/apache2/envvars'";
        $this->fileSystem->put($dockerfilePath, $dockerfile);

        $this->docker->build("$name:dev", $dockerfilePath, $root, [
            'APP_ENV' => 'dev',
            'RUN_COMPOSER' => 'yes',
        ], $this::BUILD_TARGET);
        $this->docker->push("$name:dev");
    }
}
