<?php
declare(strict_types=1);

namespace App\Strategy\Create;

class SS2ApiCreateStrategy extends K8SCreateStrategy
{
    public const CONFIG_DIRS = [
        'k8s/dev',
        'k8s/rabbitmq',
    ];
}
