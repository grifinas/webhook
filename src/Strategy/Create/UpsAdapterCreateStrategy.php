<?php
declare(strict_types=1);

namespace App\Strategy\Create;

class UpsAdapterCreateStrategy extends K8SCreateStrategy
{
    protected const BUILD_TARGET = null;
}
