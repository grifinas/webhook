<?php
declare(strict_types=1);

namespace App\Strategy\Create;

use App\Model\HookModel;

interface CreateStrategyInterface
{
    public function create(HookModel $model): HookModel;
}
