<?php
declare(strict_types=1);

namespace App\Strategy\Create;

use App\Model\HookModel;
use App\Service\Docker;
use App\Service\FileSystem;
use App\Service\Kubernetes;
use App\Service\KubernetesFileSystem;

class SS2SpaCreateStrategy extends K8SCreateStrategy
{
    private string $faToken;

    public function __construct(
        string $faToken,
        Kubernetes $kubernetes,
        Docker $docker,
        FileSystem $fileSystem,
        KubernetesFileSystem $kubernetesFileSystem
    ) {
        parent::__construct($kubernetes, $docker, $fileSystem, $kubernetesFileSystem);
        $this->faToken = $faToken;
    }

    protected function buildImages(string $root, string $name): void
    {
        $this->docker->build("$name:dev", "$root/docker/Dockerfile.dev", $root, [
            'FA_TOKEN' => $this->faToken
        ]);
        $this->docker->push("$name:dev");
    }

    public function create(HookModel $model): HookModel
    {
        parent::create($model);

        $this->editEnvFiles($model);

        return $model;
    }

    private function editEnvFiles(HookModel $model)
    {
        $file = $model->getRoot() . '/.env';
        $env = $this->fileSystem->get($file);
        $env = str_replace('venipak.local', 'venipak.megodata', $env);
        $this->fileSystem->put($file, $env);
    }
}
