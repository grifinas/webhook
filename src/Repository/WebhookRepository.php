<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Webhook;
use App\Model\HookModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Webhook|null find($id, $lockMode = null, $lockVersion = null)
 * @method Webhook|null findOneBy(array $criteria, array $orderBy = null)
 * @method Webhook[]    findAll()
 * @method Webhook[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WebhookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Webhook::class);
    }

    public function findByModel(HookModel $model): ?Webhook
    {
        return $this
            ->createQueryBuilder('w')
            ->where('w.project = :project')
            ->andWhere('w.branch = :branch')
            ->andWhere('w.name = :name')
            ->setParameters([
                'project' => $model->getProject(),
                'branch' => $model->getBranch(),
                'name' => $model->getName(),
            ])
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
