<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200224163333 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'sqlite',
            'Migration can only be executed safely on \'sqlite\'.'
        );

        $this->addSql('CREATE TABLE webhook (
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
            branch VARCHAR(255) NOT NULL, 
            project VARCHAR(255) NOT NULL, 
            name VARCHAR(255) NOT NULL, 
            link VARCHAR(255) NOT NULL, 
            root VARCHAR(255) NOT NULL,
            created_at DATETIME NOT NULL)
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'sqlite',
            'Migration can only be executed safely on \'sqlite\'.'
        );

        $this->addSql('DROP TABLE webhook');
    }
}
