<?php
declare(strict_types=1);

namespace App\Service;

use App\Exception\ValidationException;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestDeserializer
{
    private SerializerInterface $serializer;
    private ValidatorInterface $validator;

    public function __construct(SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    public function deserialize(Request $request, string $fqn): object
    {
        $model = $this->serializer->deserialize($request->getContent(), $fqn, 'json');
        $violations = $this->validator->validate($model);
        if ($violations->count()) {
            throw new ValidationException($violations);
        }

        return $model;
    }
}
