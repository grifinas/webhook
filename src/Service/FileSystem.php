<?php
declare(strict_types=1);

namespace App\Service;

use Generator;
use Symfony\Component\Filesystem\Filesystem as FileSys;
use function Symfony\Component\String\u;

/**
 * PHP function wrapper for working with file system, making testing easier
 *
 * Class FileSystemProvider
 * @package App\Service
 */
class FileSystem extends FileSys
{
    public function get(string $file): string
    {
        return file_get_contents($file);
    }

    public function put(string $file, string $data): int
    {
        return file_put_contents($file, $data);
    }

    public function getDirContents(string $dir): Generator
    {
        foreach (scandir($dir) as $item) {
            if ($item === '.' || $item === '..') {
                continue;
            }

            yield $item;
        }
    }

    public function getFiles(string $dir, string $fileEnding = ''): Generator
    {
        foreach ($this->getDirContents($dir) as $item) {
            if (u($item)->endsWith($fileEnding)) {
                yield $item;
            }
        }
    }

    public function isDir(string $dir): bool
    {
        return is_dir($dir);
    }

    public function fileExists(string $file): bool
    {
        return file_exists($file);
    }

    public function tail(string $fileName, int $lineCount): string
    {
        try {
            $file = fopen($fileName, 'r');
            $lineSizes = [];
            while ($line = fgets($file)) {
                $lineSizes[] = mb_strlen($line);
            }

            $slice = array_slice($lineSizes, max(0, count($lineSizes) - $lineCount));
            $bytes = array_reduce($slice, fn(int $total, int $current) => $total + $current, 0);

            if ($bytes <= 0) {
                return '';
            }

            fseek($file, -$bytes, SEEK_END);
            return fread($file, $bytes);
        } finally {
            fclose($file);
        }
    }
}
