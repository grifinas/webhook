<?php
declare(strict_types=1);

namespace App\Service;

use App\Exception\FileNotFoundException;
use App\Model\HookModel;
use Symfony\Component\Yaml\Yaml;
use function Symfony\Component\String\u;
use RuntimeException;

class KubernetesFileSystem
{
    private FileSystem $fileSystem;

    public function __construct(FileSystem $fileSystem)
    {
        $this->fileSystem = $fileSystem;
    }

    public function getConfigDirs(string $root, array $configDirs): array
    {
        return array_map(fn (string $dir) => "$root/$dir", $configDirs);
    }

    public function getLink(HookModel $model): string
    {
        return "{$model->getBranch()}.{$model->getName()}.venipak.megodata";
    }

    public function makeYaml(HookModel $model, string $filepath): void
    {
        $namespace = mb_strtolower($model->getBranch());

        $yaml = $this->fileSystem->get($filepath);
        $search = ['/namespace: .*/'];
        $replace = ["namespace: $namespace"];

        if (u($filepath)->endsWith('ingress.yaml')) {
            $search[] = "/[^\s]+.venipak.local/";
            $replace[] = $this->getLink($model);
        }

        $yaml = preg_replace($search, $replace, $yaml);

        $this->fileSystem->put($filepath, $yaml);
    }

    public function getConfigs(string $root, array $yamlOrder, array $confDirs): array
    {
        $configs = [];
        $sortData = array_flip($yamlOrder);
        $largest = 0;

        foreach ($this->getConfigDirs($root, $confDirs) as $dir) {
            foreach ($this->fileSystem->getFiles($dir, '.yaml') as $config) {
                $configName = substr($config, 0, -5);
                $position = ($sortData[$configName] ?? -1) + 1;
                $configs[$position][] = "$dir/$config";
                $largest = $largest < $position ? $position : $largest;
            }
        }

        $flat = [];

        $i = 0;
        while ($i <= $largest) {
            foreach ($configs[$i] ?? [] as $config) {
                $flat[] = $config;
            }
            $i++;
        }

        return $flat;
    }

    public function copySamplesToYaml(HookModel $model, string $devNamespace, array $configDirs): void
    {
        $namespace = mb_strtolower($model->getBranch());
        foreach ($this->getConfigDirs($model->getRoot(), $configDirs) as $dir) {
            foreach ($this->fileSystem->getFiles($dir, '.yaml.sample') as $config) {
                $yaml = substr($config, 0, -7);

                $sample = $this->fileSystem->get("$dir/$config");

                $search = ['{{projectRoot}}', $devNamespace];
                $replace = [$model->getRoot(), "namespace: $namespace"];

                $data = str_replace($search, $replace, $sample);

                $this->fileSystem->put("$dir/$yaml", $data);
            }
        }
    }

    public function getProjectName(HookModel $model): string
    {
        $filepath = "{$model->getRoot()}/k8s/dev/deployment.yaml";
        if (!$this->fileSystem->fileExists($filepath)) {
            throw new FileNotFoundException($filepath);
        }

        $deployment = $this->fileSystem->get($filepath);

        $deployment = Yaml::parse($deployment);

        $name = $deployment['metadata']['name'] ?? null;
        /** @var $name string */
        if (!$name) {
            throw new RuntimeException('Failed to get deployment name');
        }

        return $name;
    }
}
