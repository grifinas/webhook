<?php
declare(strict_types=1);

namespace App\Service;

use App\Model\HookModel;
use App\Model\WebHookList;
use App\Repository\WebhookRepository;
use App\Transformer\WebhookTransformer;
use Doctrine\ORM\EntityManagerInterface;

class WebhookCache
{
    private EntityManagerInterface $entityManager;
    private WebhookRepository $repository;
    private WebhookTransformer $transformer;

    public function __construct(
        EntityManagerInterface $entityManager,
        WebhookRepository $repository,
        WebhookTransformer $transformer
    ) {
        $this->entityManager = $entityManager;
        $this->repository = $repository;
        $this->transformer = $transformer;
    }

    public function get(): WebHookList
    {
        $list = new WebHookList();

        foreach ($this->repository->findAll() as $webhook) {
            $list->addHook($this->transformer->toModel($webhook));
        }
        return $list;
    }

    public function put(HookModel $hook): void
    {
        $exists = $this->repository->findByModel($hook);

        if ($exists) {
            return;
        }

        $entity = $this->transformer->toEntity($hook);

        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    public function delete(HookModel $hook): void
    {
        $entity = $this->repository->findByModel($hook);

        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }
}
