<?php
declare(strict_types=1);

namespace App\Service;

use App\Model\HookModel;
use App\Strategy\CreateStrategyProvider;
use App\Strategy\DeleteStrategyProvider;
use App\Strategy\UpdateStrategyProvider;

class ProjectManager
{
    private string $branchDir;
    private Git $git;
    private FileSystem $fileSystem;
    private WebhookCache $cache;
    private CreateStrategyProvider $createStrategies;
    private UpdateStrategyProvider $updateStrategies;
    private DeleteStrategyProvider $deleteStrategies;

    public function __construct(
        string $branchDir,
        Git $git,
        FileSystem $fileSystem,
        WebhookCache $cache,
        CreateStrategyProvider $createStrategies,
        UpdateStrategyProvider $updateStrategies,
        DeleteStrategyProvider $deleteStrategies
    ) {
        $this->branchDir = $branchDir;
        $this->git = $git;
        $this->fileSystem = $fileSystem;
        $this->cache = $cache;
        $this->createStrategies = $createStrategies;
        $this->updateStrategies = $updateStrategies;
        $this->deleteStrategies = $deleteStrategies;
    }

    public function update(HookModel $hook): void
    {
        $this->fileSystem->mkdir(sprintf('%s/%s', $this->branchDir, $hook->getProject()));

        if ($this->fileSystem->isDir($hook->getRoot())) {
            $this->git->pull($hook->getBranch(), $hook->getRoot());
            $strategy = $this->updateStrategies->get($hook);
            $hook = $strategy->update($hook);
        } else {
            $this->git->clone($hook->getProject(), $hook->getBranch(), $hook->getRoot());
            $strategy = $this->createStrategies->get($hook);
            $hook = $strategy->create($hook);
        }

        $this->cache->put($hook);
    }

    public function remove(HookModel $hook): void
    {
        if ($this->fileSystem->isDir($hook->getRoot())) {
            $strategy = $this->deleteStrategies->get($hook);
            $strategy->delete($hook);
            // permission hell to rm
            $this->fileSystem->rename($hook->getRoot(), '/tmp/' . microtime());
            $this->cache->delete($hook);
        }
    }
}
