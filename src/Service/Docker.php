<?php
declare(strict_types=1);

namespace App\Service;

class Docker
{
    private ShellExecutor $executor;

    public function __construct(ShellExecutor $executor)
    {
        $this->executor = $executor;
    }

    public function build(string $name, string $file, string $path, array $buildArgs = [], string $target = null): void
    {
        $buildArguments = [];
        $arguments = [];

        foreach ($buildArgs as $key => $value) {
            $buildArguments[] = "--build-arg \$bk_$key=\$bv_{$key}";
            $arguments["bk_$key"] = $key;
            $arguments["bv_{$key}"] = $value;
        }

        $command = 'docker build -t $target -f $file $path ';

        if ($target) {
            $arguments['buildTarget'] = $target;
            $command .= '--target=$buildTarget ';
        }

        $command .= implode(' ', $buildArguments);

        $this->executor->exec(
            $command,
            [
                'target' => "localhost:32000/$name",
                'file' => $file,
                'path' => $path,
            ] + $arguments
        );
    }

    public function push(string $name): void
    {
        $this->executor->exec('docker push $target', [
            'target' => "localhost:32000/$name",
        ]);
    }
}
