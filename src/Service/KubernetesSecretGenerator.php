<?php
declare(strict_types=1);

namespace App\Service;

class KubernetesSecretGenerator
{
    private string $privatePath;
    private string $publicPath;
    private string $knownHostsPath;
    private ShellExecutor $executor;
    private SslProvider $sslProvider;

    public function __construct(
        string $privateKeyPath,
        string $publicKeyPath,
        string $knownHostsPath,
        ShellExecutor $executor,
        SslProvider $sslProvider
    ) {
        $this->privatePath = $privateKeyPath;
        $this->publicPath = $publicKeyPath;
        $this->knownHostsPath = $knownHostsPath;
        $this->executor = $executor;
        $this->sslProvider = $sslProvider;
    }

    public function idRsa(string $namespace): self
    {
        $arguments = [
            '--from-file=id_rsa=$privatePath',
            '--from-file=id_rsa.pub=$publicPath',
            '--from-file=known_hosts=$knownHostsPath'
        ];

        $this->executor->exec(
            'microk8s.kubectl create secret generic id-rsa -n $namespace ' . implode(' ', $arguments),
            [
                'privatePath' => $this->privatePath,
                'publicPath' => $this->publicPath,
                'knownHostsPath' => $this->knownHostsPath,
                'namespace' => $namespace,
            ]
        );

        return $this;
    }

    public function ssl(string $namespace): self
    {
        $files = $this->sslProvider->get();

        $fileCount = count($files);
        $arguments = [];

        for ($i = 0; $i < $fileCount; $i++) {
            $arguments[] = "--from-file=\$$i";
        }

        $this->executor->exec(
            'microk8s.kubectl -n $namespace create secret generic ssl ' . implode(' ', $arguments),
            [
                ...$files,
                'namespace' => $namespace
            ]
        );

        return $this;
    }
}
