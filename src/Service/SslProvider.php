<?php
declare(strict_types=1);

namespace App\Service;

class SslProvider
{
    public const FILES_REQUIRED = [
        self::FILE_ROOT_CA,
        self::FILE_WILDCARD_KEY,
        self::FILE_WILDCARD_CRT
    ];

    private const FILE_ROOT_CA = 'rootCA.crt';
    private const FILE_WILDCARD_KEY = 'wildcard.venipak.key';
    private const FILE_WILDCARD_CRT = 'wildcard.venipak.crt';

    private string $sslDir;
    private ShellExecutor $executor;
    private FileSystem $fileSystem;

    public function __construct(string $sslDir, ShellExecutor $executor, FileSystem $fileSystem)
    {
        $this->executor = $executor;
        $this->sslDir = $sslDir;
        $this->fileSystem = $fileSystem;
    }

    /**
     * @return string[]
     */
    public function get(): array
    {
        $expected = array_map(fn (string $file) => "{$this->sslDir}/$file", self::FILES_REQUIRED);

        foreach ($expected as $filepath) {
            if (!$this->fileSystem->fileExists($filepath)) {
                $this->executor->exec('rm -rf $dir', [
                    'dir' => "$this->sslDir/*",
                ]);
                $this->generate();
                break;
            }
        }

        return $expected;
    }

    private function generate(): void
    {
        $commands = [
            'mkdir -p $sslDir && cd $sslDir',
            'openssl genrsa -out rootCA.key 4096',
            'openssl req -x509 -new -nodes -key rootCA.key -sha256 -days 3650 -out $rootCA -subj "/C=LT/ST=Vilnius/L=EN/O=Megodata/OU=venipak/CN=*.venipak.megodata"',
            'openssl genrsa -out $wildcardKey 2048',
            'openssl req -new -sha256 -key $wildcardKey -subj "/C=LT/ST=Vilnius/O=Megodata/CN=*.venipak.svc.cluster.local" -out wildcard.venipak.csr',
            'openssl x509 -req -in wildcard.venipak.csr -CA $rootCA -CAkey rootCA.key -CAcreateserial -out $wildcardCrt -days 1024 -sha256',
        ];

        $this->executor->exec(implode(' && ', $commands), [
            'sslDir' => $this->sslDir,
            'rootCA' => self::FILE_ROOT_CA,
            'wildcardKey' => self::FILE_WILDCARD_KEY,
            'wildcardCrt' => self::FILE_WILDCARD_CRT,
        ]);
    }
}
