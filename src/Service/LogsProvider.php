<?php
declare(strict_types=1);

namespace App\Service;

use App\Model\HookLogRequestModel;
use App\Model\HookModel;
use App\Strategy\LogsStrategyProvider;

class LogsProvider
{
    private const LINE_NUM_OUTPUT = 1000;

    private string $branchDir;
    private string $webhookLogPath;
    private FileSystem $fileSystem;
    private LogsStrategyProvider $logsStrategyProvider;

    public function __construct(
        string $branchDir,
        string $webhookLogPath,
        FileSystem $fileSystem,
        LogsStrategyProvider $logsStrategyProvider
    )
    {
        $this->branchDir = $branchDir;
        $this->webhookLogPath = $webhookLogPath;
        $this->fileSystem = $fileSystem;
        $this->logsStrategyProvider = $logsStrategyProvider;
    }

    public function getHookLogs(HookLogRequestModel $model): array
    {
        $hook = new HookModel($model->repository, $model->branch);
        $hook
            ->setName($model->project)
            ->setRoot(sprintf('%s/%s/%s', $this->branchDir, $model->repository, $model->branch))
        ;

        $strategy = $this->logsStrategyProvider->get($hook);
        return $strategy->getLogs($hook);
    }

    public function getWebhookLog(): string
    {
        $lines = str_replace("\n", ',', trim($this->fileSystem->tail($this->webhookLogPath, self::LINE_NUM_OUTPUT)));

        return sprintf('[%s]', $lines);
    }
}
