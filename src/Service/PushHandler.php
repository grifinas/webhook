<?php
declare(strict_types=1);

namespace App\Service;

use App\Model\EventModel;
use App\Model\HookModel;
use App\Model\ReferenceStateModel;

class PushHandler
{
    private ProjectManager $projectManager;
    private string $branchDir;

    public function __construct(string $branchDir, ProjectManager $projectManager)
    {
        $this->branchDir = $branchDir;
        $this->projectManager = $projectManager;
    }

    public function handle(EventModel $model): void
    {
        $updatingBranches = $this->getBranchesForUpdate($model);

        foreach ($updatingBranches as $branch) {
            $hook = (new HookModel($model->repository->name, $branch))->setRoot($this->branchDir);
            $this->projectManager->update($hook);
        }
    }

    /**
     * @param EventModel $model
     * @return string[]
     */
    private function getBranchesForUpdate(EventModel $model): array
    {
        $changedBranches = [];
        foreach ($model->push->changes as $change) {
            if ($change->new->type === ReferenceStateModel::TYPE_BRANCH) {
                $changedBranches[] = $change->new->name;
            }
        }

        return array_unique($changedBranches);
    }
}
