<?php
declare(strict_types=1);

namespace App\Service;

use ArrayAccess;

class Collection implements ArrayAccess
{
    private array $collection;

    public function __construct(array $elements = [])
    {
        $this->collection = $elements;
    }

    public function add($element): bool
    {
        $this->collection[] = $element;
        return true;
    }

    /**
     * Sets an element in the collection at the specified key/index.
     *
     * @param string|int $key The key/index of the element to set.
     * @param mixed $value The element to set.
     *
     * @return void
     */
    public function set($key, $value): void
    {
        $this->collection[$key] = $value;
    }

    /**
     * Gets the element at the specified key/index.
     *
     * @param string|int $key The key/index of the element to retrieve.
     *
     * @return mixed
     *
     * @psalm-param TKey $key
     * @psalm-return T|null
     */
    public function get($key)
    {
        return $this->collection[$key] ?? null;
    }

    /**
     * Removes the element at the specified index from the collection.
     *
     * @param string|int $key The key/index of the element to remove.
     *
     * @return mixed The removed element or NULL, if the collection did not contain the element.
     *
     * @psalm-param TKey $key
     * @psalm-return T|null
     */
    public function remove($key)
    {
        if (!isset($this->collection[$key]) && !array_key_exists($key, $this->collection)) {
            return null;
        }

        $removed = $this->collection[$key];
        unset($this->collection[$key]);

        return $removed;
    }

    /**
     * Checks whether an element is contained in the collection.
     * This is an O(n) operation, where n is the size of the collection.
     *
     * @param mixed $element The element to search for.
     *
     * @return bool TRUE if the collection contains the element, FALSE otherwise.
     *
     * @psalm-param T $element
     */
    public function contains($element): bool
    {
        return in_array($element, $this->collection, true);
    }

    /**
     * Checks whether the collection contains an element with the specified key/index.
     *
     * @param string|int $key The key/index to check for.
     *
     * @return bool TRUE if the collection contains an element with the specified key/index,
     *              FALSE otherwise.
     *
     * @psalm-param TKey $key
     */
    public function containsKey($key): bool
    {
        return isset($this->collection[$key]) || array_key_exists($key, $this->collection);
    }

    /**
     * Required by interface ArrayAccess.
     *
     * {@inheritDoc}
     */
    public function offsetExists($offset)
    {
        return $this->containsKey($offset);
    }

    public function list(): array
    {
        return $this->collection;
    }

    /**
     * Required by interface ArrayAccess.
     *
     * {@inheritDoc}
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * Required by interface ArrayAccess.
     *
     * {@inheritDoc}
     */
    public function offsetSet($offset, $value)
    {
        if (!isset($offset)) {
            $this->add($value);

            return;
        }

        $this->set($offset, $value);
    }

    /**
     * Required by interface ArrayAccess.
     *
     * {@inheritDoc}
     */
    public function offsetUnset($offset)
    {
        $this->remove($offset);
    }

    public function removeElement($element) : bool
    {
        $key = array_search($element, $this->collection, true);

        if ($key === false) {
            return false;
        }

        unset($this->collection[$key]);

        return true;
    }
}
