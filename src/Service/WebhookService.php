<?php
declare(strict_types=1);

namespace App\Service;

use App\Exception\WebhookHandlerException;
use App\Model\HookModel;
use App\Repository\WebhookRepository;
use App\Transformer\WebhookTransformer;

class WebhookService
{
    private string $branchDir;
    private FileSystem $fileSystem;
    private ProjectManager $projectManager;
    private Git $git;
    private WebhookRepository $repository;
    private WebhookTransformer $transformer;

    public function __construct(
        string $branchDir,
        FileSystem $fileSystem,
        ProjectManager $projectManager,
        Git $git,
        WebhookRepository $repository,
        WebhookTransformer $transformer
    ) {
        $this->branchDir = $branchDir;
        $this->fileSystem = $fileSystem;
        $this->projectManager = $projectManager;
        $this->git = $git;
        $this->repository = $repository;
        $this->transformer = $transformer;
    }

    public function getBranches(string $repository): array
    {
        return $this->git->getRepositoryBranches(mb_strtolower($repository), $this->branchDir);
    }

    public function getProjects(): array
    {
        return iterator_to_array($this->fileSystem->getDirContents($this->branchDir));
    }

    public function create(string $repository, string $branch): void
    {
        $webhook = $this->repository->findOneBy([
            'branch' => $branch,
            'project' => $repository,
        ]);

        if ($webhook) {
            throw new WebhookHandlerException(sprintf('Webhook %s %s already exists', $repository, $branch));
        }

        $model = (new HookModel($repository, $branch))->setRoot(
            sprintf('%s/%s/%s', $this->branchDir, $repository, $branch)
        );

        $this->projectManager->update($model);
    }

    public function delete(string $repository, string $branch): void
    {
        $webhook = $this->repository->findOneBy([
            'branch' => $branch,
            'project' => $repository,
        ]);

        if ($webhook === null) {
            throw new WebhookHandlerException(sprintf('Webhook not found %s %s', $repository, $branch));
        }

        $hook = $this->transformer->toModel($webhook);
        $this->projectManager->remove($hook->setRoot(sprintf('%s/%s/%s', $this->branchDir, $repository, $branch)));
    }

    public function recreate(string $repository, string $branch): void
    {
        $this->delete($repository, $branch);
        $this->create($repository, $branch);
    }
}
