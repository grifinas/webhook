<?php
declare(strict_types=1);

namespace App\Service;

use App\Exception\PodStateException;
use App\Model\ExecResponseModel;
use App\Model\KubernetesPodModel;
use Exception;
use Symfony\Component\Dotenv\Dotenv;
use function Symfony\Component\String\u;

class Kubernetes
{
    private ShellExecutor $executor;
    private KubernetesSecretGenerator $secretGenerator;

    public function __construct(
        ShellExecutor $executor,
        KubernetesSecretGenerator $secretGenerator
    ) {
        $this->executor = $executor;
        $this->secretGenerator = $secretGenerator;
    }

    public function apply(string $file): void
    {
        $this->executor->exec('microk8s.kubectl apply -f $file', [
            'file' => $file,
        ]);
    }

    public function delete(string $file): void
    {
        $this->executor->exec('microk8s.kubectl delete -f $file', [
            'file' => $file,
        ]);
    }

    public function deleteNamespace(string $namespace): void
    {
        $this->executor->exec('microk8s.kubectl delete namespace $namespace', [
            'namespace' => mb_strtolower($namespace),
        ]);
    }

    public function createNamespace(string $namespace): void
    {
        $this->executor->exec('microk8s.kubectl create namespace $namespace', [
            'namespace' => mb_strtolower($namespace),
        ]);
    }

    public function generateSecret(): KubernetesSecretGenerator
    {
        return $this->secretGenerator;
    }

    public function findPod(string $podName, string $namespace): ?KubernetesPodModel
    {
        $namespace = mb_strtolower($namespace);

        $result = $this->executor->exec('microk8s.kubectl -n $namespace get pods', [
            'namespace' => $namespace,
        ]);

        $pods = $this->extractPods($result, $namespace);

        foreach ($pods as $pod) {
            if (u($pod->name)->startsWith(mb_strtolower($podName))) {
                return $pod;
            }
        }

        return null;
    }

    public function getPod(string $podName, string $namespace): KubernetesPodModel
    {
        $pod = $this->findPod($podName, $namespace);

        if (!$pod) {
            throw new Exception('pod not found');
        }

        if ($pod->status !== KubernetesPodModel::STATUS_RUNNING) {
            throw new PodStateException('Pod is not running');
        }

        return $pod;
    }

    public function getContainerLog(KubernetesPodModel $pod, string $containerName = ''): array
    {
        $logs = [];

        $result = $this->executor->exec('microk8s.kubectl logs -n $namespace $podName $containerName', [
            'namespace' => $pod->namespace,
            'podName' => $pod->name,
            'containerName' => $containerName,
        ]);

        if ($result->output) {
            foreach ($result->output as $line) {
                $logs[] = $line;
            }
        }

        return $logs;
    }

    public function runMigrations(KubernetesPodModel $pod): ExecResponseModel
    {
        return $this->executor->exec(
            'microk8s.kubectl -n $namespace exec $pod -- bin/console doctrine:migrations:migrate --allow-no-migration -n',
            [
                'namespace' => $pod->namespace,
                'pod' => $pod->name,
            ]
        );
    }

    public function getEnv(KubernetesPodModel $pod): array
    {
        $response = $this->executor->exec(
            'microk8s.kubectl -n $namespace exec $pod -- printenv',
            [
                'namespace' => $pod->namespace,
                'pod' => $pod->name,
            ]
        );

        $output = [];
        foreach ($response->output as $row) {
            if (strpos($row, '=') === false) {
                continue;
            }

            $output[] = preg_replace('/=(.*)$/', '="$1"', $row);
        }

        return (new Dotenv())->parse(implode("\n", $output));
    }

    public function restartApache(KubernetesPodModel $pod): void
    {
        $this->executor->exec('microk8s.kubectl -n $namespace exec $pod -- service apache2 restart', [
            'namespace' => $pod->namespace,
            'pod' => $pod->name,
        ]);
    }

    /**
     * @param string $namespace
     * @return KubernetesPodModel[]
     */
    public function getNamespacePods(string $namespace): array
    {
        $pods = $this
            ->executor
            ->exec('microk8s.kubectl get pods -n $namespace --no-headers=true', ['namespace' => $namespace]);

        return $this->extractPods($pods, $namespace);
    }

    /**
     * @param ExecResponseModel $execModel
     * @param string $namespace
     * @return KubernetesPodModel[]
     */
    private function extractPods(ExecResponseModel $execModel, string $namespace): array
    {
        $pods = [];

        if ($execModel->result !== ExecResponseModel::RESULT_OK) {
            return [];
        }

        foreach ($execModel->output as $line) {
            $matches = [];
            preg_match_all('/[^\s]+/', $line, $matches);
            $name = $matches[0][0] ?? '';

            $pods[] = new KubernetesPodModel($name, $matches[0][2], mb_strtolower($namespace));
        }


        return $pods;
    }
}
