<?php
declare(strict_types=1);

namespace App\Service;

use App\Model\ExecResponseModel;
use Psr\Log\LoggerInterface;

class ShellExecutor
{
    private LoggerInterface $webhookLogger;

    public function __construct(LoggerInterface $webhookLogger)
    {
        $this->webhookLogger = $webhookLogger;
    }

    /**
     * @param string $command
     * @param array<string, string> $args
     * @return ExecResponseModel
     */
    public function exec(string $command, array $args = []): ExecResponseModel
    {
        $search = [];
        $replace = [];

        foreach ($args as $key => $value) {
            $search[] = "$$key";
            $replace[] = escapeshellarg($value);
        }

        $sanitizedCommand = str_replace($search, $replace, $command) . ' 2>&1';

        $output = [];
        $result = 0;
        exec($sanitizedCommand, $output, $result);
        $this->webhookLogger->info('Logging command', [
            'command' => $command,
            'args' => $args,
            'sanitized' => $sanitizedCommand,
            'response' => $result,
            'output' => $output,
        ]);

        return new ExecResponseModel($result, $output);
    }
}
