<?php
declare(strict_types=1);

namespace App\Service;

use App\Exception\FileNotFoundException;

class Git
{
    private string $host;
    private string $owner;
    private ShellExecutor $executor;

    public function __construct(string $gitHost, string $gitOwner, ShellExecutor $executor)
    {
        $this->host = $gitHost;
        $this->owner = $gitOwner;
        $this->executor = $executor;
    }

    public function clone(string $repository, string $branch, string $dir): void
    {
        $this->executor->exec(
            'git clone --single-branch --branch $branch $repository $dir',
            [
                'branch' => $branch,
                'repository' => "git@{$this->host}:{$this->owner}/$repository.git",
                'dir' => $dir,
            ]
        );
    }

    public function pull(string $branch, string $dir): void
    {
        $this->executor->exec('cd $dir && git fetch && git reset --hard $branch', [
            'dir' => $dir,
            'branch' => "origin/$branch",
        ]);
    }

    public function getRepositoryBranches(string $repository, string $dir): array
    {
        $branches = $this->executor->exec('ls $dir', [
            'dir' => "$dir/$repository",
        ]);

        if (strpos($branches->output[0], 'No such file or directory') !== false ) {
            throw new FileNotFoundException('No such file or directory');
        }

        if (!in_array('master', $branches->output)) {
            throw new FileNotFoundException('No master branch found');
        }

        return $this->executor->exec('cd $dir && git ls-remote --heads origin | sed \'s?.*refs/heads/??\'', [
            'dir' => "$dir/$repository/master",
        ])->output;

    }
}
