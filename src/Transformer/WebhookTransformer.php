<?php
declare(strict_types=1);

namespace App\Transformer;

use App\Entity\Webhook;
use App\Model\HookModel;

class WebhookTransformer
{
    public function toEntity(HookModel $model): Webhook
    {
        return (new Webhook())
            ->setName($model->getName())
            ->setBranch($model->getBranch())
            ->setProject($model->getProject())
            ->setLink($model->getLink())
            ->setCreatedAt($model->getCreatedAt())
            ->setRoot($model->getRoot())
        ;
    }

    public function toModel(Webhook $entity): HookModel
    {
        $model = new HookModel(
            $entity->getProject(),
            $entity->getBranch(),
            $entity->getCreatedAt()
        );
        return $model
            ->setId($entity->getId())
            ->setName($entity->getName())
            ->setLink($entity->getLink())
            ->setRoot($entity->getRoot())
        ;
    }
}
