## Setup

Get symfony binary [from here](https://symfony.com/download)

Get yarn [instructions](https://classic.yarnpkg.com/en/docs/install#debian-stable)
```
sudo apt-get install sqlite3
yarn
yarn encore dev
bin/console doctrine:database:create
bin/console doctrine:migrations:migrate
symfony serve
```
